package org.njk.statemachine

import java.util.*


sealed class State(val name: String) {
    override fun toString() = name
    object START : State("start")

    object CREATED : State("created")

    object ACCEPTED : State("accepted")

    object REJECTED : State("rejected")

    object CANCELLED : State("cancelled")

    object VIEWED : State("viewed")

    object SETTLED_OUTSIDE : State("settled_outside")

    object END : State("end")

    companion object {
        private val allStates = hashMapOf("start" to START,
            "created" to CREATED, "accepted" to ACCEPTED,
            "rejected" to REJECTED, "cancelled" to CANCELLED,
            "viewed" to VIEWED, "settled_outside" to SETTLED_OUTSIDE,
            "end" to END)

        fun lookup(state: String) = allStates.get(state)
    }
}

data class Event(val name: String)

data class Transition(var begin: State? = null, var end: State? = null, var event: Event? = null)

data class Context<Any>(val ctx: HashMap<String, Any>)

class Document(val docNumber: String, val docType: String) {
    var state: String = State.START.name
}
