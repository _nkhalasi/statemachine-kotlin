package org.njk.statemachine

import java.util.*


class StateProcessor(val name: String, val transitions: MutableList<Transition>) {

    fun process(obj: Document, event: Event, context: Context<Any> = Context(HashMap<String, Any>())): Transition? {
        val transition = identifyTransition(event, context)

        transition?.let {
            obj.state = it.end!!.name
            //TODO: Execute functions
            return it
        }
        return null
    }

    private fun identifyTransition(event: Event, context: Context<Any> = Context(HashMap<String, Any>())): Transition? {
        val x = transitions.filter { it.event?.name == event.name }
        //TODO: Select based on predicate
        return if (x.size > 1) x[0] else null
    }
}

class StateMachine(val name: String, val states: List<State>) {
    val processors = lazy {
        val ppairs = states.map({ it to StateProcessor(it.name, mutableListOf<Transition>()) })
        hashMapOf(*ppairs.toTypedArray())
    }

    fun registerTransition(transition: Transition) {
        processors.value.get(transition.begin)!!.transitions.add(transition)
    }

    tailrec fun process(obj: Document, event: Event, context: Context<Any> = Context(HashMap<String, Any>())) {
        val tr = processors.value.get(State.lookup(obj.state))?.process(obj, event, context)
        if (tr != null)
            process(obj, event, context)
    }

}
