package org.njk.statemachine


inline fun Transition.goFrom(state: State, builder: Transition.() -> Unit) {
    begin = state
    builder()
}

inline fun Transition.to(state: State) {
    end = state
}

inline fun StateMachine.on(event: Event, builder: Transition.() -> Unit): Transition {
    val tr = Transition(event = event)
    tr.builder()
    registerTransition(tr)
    return tr
}

fun statemachine(name: String, states: List<State>, builder: StateMachine.() -> Unit): StateMachine {
    val sm = StateMachine(name, states)
    sm.builder()
    return sm
}
