package org.njk.statemachine


fun main(args: Array<String>) {

    val docStateMachine = statemachine("Document StateMachine",
        listOf(State.START, State.CREATED, State.VIEWED, State.ACCEPTED,
            State.CANCELLED, State.REJECTED, State.SETTLED_OUTSIDE)) {
        on(Event("document_created")) {
            goFrom(State.START) {
                to(State.CREATED)
            }
        }
        on(Event("document_viewed")) {
            goFrom(State.CREATED) {
                to(State.VIEWED)
            }
        }
        on(Event("document_approved")) {
            goFrom(State.CREATED) {
                to(State.ACCEPTED)
            }
        }
        on(Event("document_approved")) {
            goFrom(State.VIEWED) {
                to(State.ACCEPTED)
            }
        }
        on(Event("document_rejected")) {
            goFrom(State.CREATED) {
                to(State.REJECTED)
            }
        }
        on(Event("document_cancelled")) {
            goFrom(State.CREATED) {
                to(State.CANCELLED)
            }
        }
        on(Event("document_settled_outside")) {
            goFrom(State.CREATED) {
                to(State.SETTLED_OUTSIDE)
            }
        }
        on(Event("document_cancelled")) {
            goFrom(State.CANCELLED) {
                to(State.END)
            }
        }
        on(Event("document_rejected")) {
            goFrom(State.REJECTED) {
                to(State.END)
            }
        }
        on(Event("document_settled_outside")) {
            goFrom(State.SETTLED_OUTSIDE) {
                to(State.END)
            }
        }
    }
    docStateMachine.processors.value.forEach { k, v -> println("$k :: ${v.transitions}") }
}
