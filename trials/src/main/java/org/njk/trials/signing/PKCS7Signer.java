package org.njk.trials.signing;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;

public final class PKCS7Signer {

   
    private static final String SIGNATUREALGO = "SHA1withRSA";
    
    private KeyStore keyStore ;
    private CMSSignedDataGenerator signatureGenerator ;
    private String keyAlias;
    private String storePassword;

    /**
     * 
     * @param pathToKeystore Path to Keystore (.p12 file)
     * @param keyAlias Alias for the person identity
     * @param storePassword Password to access the keystore
     * @throws Exception Any exception
     */
    public PKCS7Signer(String pathToKeystore,String keyAlias,String storePassword) throws Exception {
    	System.out.println("PKCS7Signer (pathToKeystore = "+pathToKeystore+", keyAlias = "+keyAlias+", storePassword = "+storePassword+") ctor");
    	this.keyAlias = keyAlias;
    	this.storePassword = storePassword;
    	this.keyStore = loadKeyStore(pathToKeystore,keyAlias,storePassword);
        this.signatureGenerator = setUpProvider(keyStore,keyAlias,storePassword);
    }

    private static KeyStore loadKeyStore(String pathToKeystore,String alias, String storePassword) throws Exception {

        KeyStore keystore = KeyStore.getInstance("PKCS12");
        InputStream is = new FileInputStream(pathToKeystore);
        keystore.load(is, storePassword.toCharArray());
        
        Certificate cert = keystore.getCertificate(alias);
        System.out.println(cert.getType()+" : "+cert.getPublicKey());
        return keystore;
    }

    private static CMSSignedDataGenerator setUpProvider(final KeyStore keystore,String keyAlias,String storePassword) throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        Certificate[] certchain = (Certificate[]) keystore.getCertificateChain(keyAlias);

        final List<Certificate> certlist = new ArrayList<Certificate>();

        for (int i = 0, length = certchain == null ? 0 : certchain.length; i < length; i++) {
            certlist.add(certchain[i]);
        }

        Store certstore = new JcaCertStore(certlist);

        Certificate cert = keystore.getCertificate(keyAlias);

        ContentSigner signer = new JcaContentSignerBuilder(SIGNATUREALGO).setProvider("BC").build((PrivateKey) (keystore.getKey(keyAlias, storePassword.toCharArray())));

        CMSSignedDataGenerator generator = new CMSSignedDataGenerator();

        generator.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().setProvider("BC").
                build()).build(signer, (X509Certificate) cert));

        generator.addCertificates(certstore);

        return generator;
    }

    public byte[] signPkcs7(final byte[] signature) throws Exception {

       return  signPkcs7(signature, true);
    }
    
    public byte[] signPkcs7(final byte[] content,boolean detached) throws Exception {

        CMSTypedData cmsdata = new CMSProcessableByteArray(content);
        CMSSignedData signeddata = signatureGenerator.generate(cmsdata, detached);
        
        return signeddata.getEncoded();
    }
    
    
       
    
    public PrivateKey getPrivateKey() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
      return (PrivateKey) this.keyStore.getKey(this.keyAlias, this.storePassword.toCharArray());
    }

   
}