package com.vayana.vnet.vstry

import arrow.core.Option
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

typealias Organisation = String
typealias DocumentType = String
typealias DocumentNumber = String
typealias IssueDate = Date
typealias Amount = BigDecimal
typealias WorkflowId = String
typealias AttributeId = String


data class RemittanceInformation(val name: String, val ifscCode: String, val number: String)

data class Tenor(val number: Int, val unit: TimeUnit)

enum class FRTriggerType {
  Manual,
  Immediate,
  Deferred
}

enum class SignatureMandate {
  NotRequired,
  Required,
  VerifiedAtBank
}

interface DynamicValue<T> {
  val id: AttributeId
  val klass: Class<T>
  val value: T
}

interface OrganisationDocTypeRequiredAttributes {
  val documentType: DocumentType
  val requiredAttributes: List<DynamicValue<*>>
}

/* --------------------------------------------------------------------------- */

interface Attachment {

}

interface Document: Attachment {
  val originator: Organisation
  val recipient: Organisation
  val buyer: Organisation // auto computed
  val seller: Organisation // auto computed
  val type: DocumentType
  val number: DocumentNumber
  val issueDate: IssueDate
  val amount: Amount
}

interface DocumentWithFinancier: Document {
  val financier: Organisation
}

interface Fundable {
  val attachments: List<Attachment>
}

/* --------------------------------------------------------------------------- */
interface DocumentStateMachine {
  val participatingDocumentTypes: List<DocumentType>
  val fundableType: DocumentType
  fun acceptanceRequiredForDocumentType(dt: DocumentType): Boolean
}

interface Workflow {
  /* collaboration built by default. Has 3 characteristics
  ** 1. Chat channel
  ** 2. Upload attachments
  ** 3. Approve event
   */
  val id: WorkflowId
  val name: String
  val stateMachine: DocumentStateMachine
  val allowCollaboration: Boolean
  // some way of specifying triggering card / nodal on fr events
}

interface FinancingWorkflow: Workflow

/* --------------------------------------------------------------------------- */

interface Contract

interface DocExchangeContract: Contract {
  val seller: Organisation
  val buyer: Organisation
}

interface FinancingContract: Contract {
  val workflow: FinancingWorkflow
  val borrower: Organisation
  val borrowerProxies: List<Organisation>
  val borrowerIsBuyer: Boolean
  val financier: Organisation
  val financierProxies: List<Organisation>
  val tenor: Tenor
  val triggerType: FRTriggerType
  val preverifiedDocumentTypes: List<DocumentType>
  val signatureMandates: Map<DocumentType, SignatureMandate>
}

interface FinancingContractWoCP: FinancingContract {
}

interface FinancingContractWithCP: FinancingContract {
  val counterparty: Organisation
  val counterpartyProxies: List<Organisation>
}

interface ContractResolver {
  fun resolve(initiator: Organisation, document: Document): Option<Contract>
  // Three possibilities
  // Borrower has a single contract in the system
  // Borrower has multiple contracts, but financier is specified in the contract
  // Borrower and counterparty collectively
  // Borrower, counterparty and financier used for resolution
}

/* --------------------------------------------------------------------------- */

interface PayoutContract: Contract {
  val payoutTarget: Organisation
  val financier: Organisation
  val debitAccount: RemittanceInformation
}

interface PayoutContractWithoutCreditAccount: PayoutContract {
}

interface PayoutContractWithCreditAccount: PayoutContract {
  val creditAccount: RemittanceInformation
}

/* --------------------------------------------------------------------------- */

interface NodalAccountDetails
interface MerchantDetails

interface NodalContract: Contract {
  val paymentFacilitator: Organisation
  val cardAcquirer: Organisation
  val buyer: Organisation
  val merchant: Organisation

  val cardAcquirerChargeRate: BigDecimal
  val paymentFacilitatorChargeRate: BigDecimal
}

/* --------------------------------------------------------------------------- */

interface CardContract: Contract {

}