package org.njk.trials.typing.typealiases

import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.text.RandomStringGenerator

object Passwords {
    val generator = RandomStringGenerator.Builder().withinRange("a".codePointAt(0),"z".codePointAt(0)).build()
    fun hash(password: String): String =
        generator.generate(6).let { salt -> salt + DigestUtils.sha256Hex(salt+password) }

    fun verify(password: String, hash: String): Boolean =
        hash.substring(0,6).let { salt -> salt + DigestUtils.sha256Hex(salt+password) }.equals(hash)
}

typealias UserName = String
typealias Password = String
typealias PasswordHash = String
typealias HashFunction = (Password) -> PasswordHash

data class Credentials(val username: UserName, val passwordHash: PasswordHash)

fun HashFunction.makeCredentials(username: UserName, password: Password): Credentials =
    Credentials(username, this.invoke(password))

val md5: HashFunction = { pwd ->
    Passwords.generator.generate(6).let { salt ->
        salt + DigestUtils.md5Hex(salt+pwd)
    }
}

typealias CredentialsBuilder = (UserName, Password) -> Credentials

fun hashingCredentialsBuilder(fn: HashFunction): CredentialsBuilder = { u, p ->
    Credentials(u, fn(p))
}

val md5Credentials = hashingCredentialsBuilder { p ->
    md5(p)
}

typealias Prompt = String
typealias Input = String
typealias IOContext = (Prompt) -> Input

//An IOContext that reads from the console
val consoleIOContext: IOContext = { prompt ->
    print("$prompt : > ")
    readLine()!!
}

//Validator to check that an input matches an expected type
typealias Validator<T> = (Input) -> T

//An InputReader obtains a value from an IOContext
typealias InputReader<T> = (IOContext) -> T

fun <T> validatingInputReader(prompt: Prompt, validator: Validator<T>): InputReader<T> = { iocontext ->
    validator(iocontext(prompt))
}

fun <A, B, C> InputReader<A>.combine(other: InputReader<B>, combineFn: (A, B) -> C): InputReader<C> = { ioContext ->
    combineFn(this(ioContext), other(ioContext))
}


fun main(args: Array<String>) {
    val c = md5.makeCredentials("naresh", "Password@12")
    println(c)

    println(md5Credentials("krishang", "Confidential@12"))

    val usernameValidator: Validator<UserName> = { it } //returns the value as is without validating
    val passwordValidator: Validator<Password> = { it } //returns the value as is without validating
    val usernameInputReader: InputReader<UserName> = validatingInputReader("Enter Username", usernameValidator)
    val passwordInputReader: InputReader<Password> = validatingInputReader("Enter Password", passwordValidator)

//    println(usernameInputReader(consoleIOContext))
//    println(passwordInputReader(consoleIOContext))

    val md5CredentialsInputReader = usernameInputReader.combine(passwordInputReader, md5Credentials)
    println(md5CredentialsInputReader(consoleIOContext))

}