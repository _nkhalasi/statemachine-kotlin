package org.njk.trials.typing.understanding_variances

import java.util.*

//Invariance describes a relationship between two sets of types where the complex types do not subtype at all, despite any subtyping that might exist for the component types.

class Box<T>(val item: T)
/*
Box is invariant on type parameter T
By default, generics in Kotlin (and Java) are invariant – if you don’t specify any variance annotation, like in or out, a generic class will be invariant.
 */

class ComplicatedBox<out T, in U, V>
/*
In this case:
  - ComplicatedBox is covariant on type parameter T
  - ComplicatedBox is contravariant on type parameter U
  - ComplicatedBox is invariant on type parameter V
 */

/*
- Properties are invariant when they are declared with var, because it effectively results in both a getter and a setter for the property,
which puts the type in both the “in” (setter) and “out” (getter) positions.
- In Kotlin and Java, function arguments are invariant.
If a subclass specifies a more general argument type than its superclass, the function will be overloaded, not overridden.
- Function return types are not invariant - they’re covariant, so a subclass can declare a return type that’s more specific than its superclass.
 */


//Covariance describes a relationship between two sets of types where they both subtype in the same direction.

/*
And in case you’re wondering, in Kotlin:
  - A read-only List is indeed covariant on its type parameter. So List<B> is a subtype of List<A>.
  - A MutableList, however, is not covariant, so MutableList<B> is not a subtype of MutableList<A>.
 */
open class A
open class B : A()
val listOfB: List<B> = listOf(B())
val listOfA: List<A> = listOfB

fun read(list: List<A>) {
  list.forEach { println(it) }
}
val x = read(listOfB)

fun readBox(box: Box<A>) {
  println("Box<A>(hashcode=${box.hashCode()})")
}
val box: Box<B> = Box(B())
//val xx = readBox(box) //won't compile because class Box is invariant on type T and we need it to be covariant (B is subtype of A and Box<B> is subtype of Box<A>)

class CovariantBox<out T>(val item : T)
/* B is subtype of A and CovariantBox<B> is a subtype of CovariantBox<A> */
fun readCovariantBox(box: CovariantBox<A>) {
  println("CovariantBox(hashcode=${box.hashCode()}")
}
val covarBox: CovariantBox<B> = CovariantBox(B())
val xx = readCovariantBox(covarBox)
/*
Covariance doesn’t just apply to generics. When you override a function in a subclass, you can declare a return type that’s more specific than the superclass declares.
For example, if you’ve got a superclass with a function that returns an A, it’s legal for a subclass to declare that it returns a B instead.
 */
open class SuperClass {
  open fun getValue(): A = A()
}

open class SubClass: SuperClass() {
  override fun getValue(): B = B()
}

//Contravariance describes a relationship between two sets of types where they subtype in opposite directions.

class ContravariantDropbox<in T> {
  private val items = mutableListOf<T>()
  fun deposit(item: T) = items.add(item)
}
/* B is a subtype of A, whereas ContravariantDropbox<B> is supertype of ContravariantDropbox<A> */
val contravarBoxOfA: ContravariantDropbox<A> = ContravariantDropbox<A>()
val contravarBoxOfB: ContravariantDropbox<B> = contravarBoxOfA

fun update(box: ContravariantDropbox<B>) {
  //something here
}
val xxx = update(contravarBoxOfA)

/*
open class Super {
  open fun execute(arg: B): Unit = TODO()
}

class Sub : Super() {
  override fun execute(arg: A): Unit = TODO()
}

By the rules of contravariance, this is type-safe. But the Kotlin compiler will error on Sub’s override, saying:
    execute overrides nothing

Why would language designers choose to keep argument types invariant? The trade-off has to do with function overloading.
*/

open class Super {
  open fun execute(arg: B): Unit = println("Super::execute($arg)")
  open val executeFn: (B) -> Unit = { arg -> println("Super::executeFn($arg)") }
}

class Sub : Super() {
  fun execute(arg: A): Unit = println("Sub::execute($arg)")
  override val executeFn: (A) -> Unit = { arg -> println("Sub::executeFn($arg)") }
}

fun funArgsAreInvariants() =
  with(Sub()) {
    execute(A())        //invoke Sub's execute()
    execute(B())        //invoke Super's execute()
    execute((B() as A)) //invoke Sub's execute()
  }


fun propsCanBeContravariants() =
    with(Sub()) {
       executeFn(A())        //invoke Sub's executeFn()
       executeFn(B())        //invoke Sub's executeFn()
       executeFn((B() as A)) //invoke Sub's executeFn()
  }

fun main() {
  println("---- func args are invariants ----")
  funArgsAreInvariants()
  println("---- properties are contravariants ----")
  propsCanBeContravariants()
  println("---- ---- ---- ---- ----")
}

interface Animal {
  val name: String
}
data class Dog(override val name: String) : Animal
data class Cat(override val name: String) : Animal

// Out-projection - makes a generic covriant

class VetClinic {
  fun examine(patients: Array<out Animal>) = println("The first patient is ${patients[0].name}")
}
/* By adding the out variance annotation to the type argument Animal, our VetClinic can now accept an Array<Dog> or Array<Cat> in addition to Array<Animal> */
val dogs: Array<Dog> = arrayOf(Dog("Bruno"), Dog("Bandit"))
val a = VetClinic().examine(dogs)
/* When you’re working with an out-projection, you’re not allowed to use any functions or setters that would accept that type parameter.
So in other words, our examine() function is not allowed to put an Animal into the array.*/


// In-projection - makes a generic contravariant, but also causes any functions that return the type parameter to return it as a more abstract type

fun Dog.addToQueue(queue: Queue<in Dog>): Unit {
  val nextDog = queue.peek() //this returns Any? instead of Dog, you have lost the type because of in-projection
  queue.add(this)
}
val bruno = Dog("bruno")
val dogQueue: Queue<Dog> = LinkedList()
val a1 = bruno.addToQueue(dogQueue)

val animalQueue: Queue<Animal> = LinkedList()
val a2 = bruno.addToQueue(animalQueue)
/* If you were to remove in from the type argument, this last line would no longer compile, because Queue<Animal> isn’t normally a subtype of Queue<Dog>. */

class InvariantBox<T>(private var item: T) {
  fun getItem(): T = item
  fun setItem(newItem: T): Unit { item = newItem }
}

//COVARIANT
class ReadOnlyBox<out T>(private var item: T) {
  fun getItem():T = item
//  fun setItem(newItem: T): Unit = TODO()    // not allowed - won't compile
}

//CONTRAVARIANT
class WriteOnlyBox<in T>(private var item: T) {
  fun setItem(newItem: T): Unit { item = newItem }
//  fun getItem(): T = item         // not allowed - won't compile
}

interface WriteableGroup<in T> {
  fun insert(item: T): Unit
}
interface ReadableGroup<out T> {
  fun fetch(): T
}
interface Group<T> : ReadableGroup<T>, WriteableGroup<T>

fun read(group: Group<out Dog>) = println(group.fetch())
fun write(group: Group<in Dog>) = group.insert(Dog("bruno"))