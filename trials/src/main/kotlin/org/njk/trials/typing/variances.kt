package org.njk.trials.typing.variances

import kotlin.random.Random

/*
When you want subtyping to apply to your generic types (that’s what we call generic variance), Kotlin gives you two options:
 * Declaration-site variance - you can specify the variance within the definition of the class or interface.
 * Use-site variance - you can specify the variance at the places in your code where you’re using an instance of a generic.

Type projection is the result of use-site variance. When you apply use-site variance to an object, that object’s type is projected to a more limited view of the type.
 */

open class A
open class B : A()

interface Animal {
  val name: String
}
interface Named {
  val species: String
}
data class Dog(override val name: String, override val species: String) : Animal, Named
data class Cat(override val name: String) : Animal

data class Box<T>(var item: T)
/*
We say this class is invariant, because:
  * Box<Dog> is not a subtype of Box<Animal>, and
  * Box<Animal> is not a subtype of Box<Dog>

Now, it’s important to note that there are two ways that you can interact with an item in this Box:
  * You can get the item out of the Box
  * You can set the item in to the Box

However, at the location where we’re using Box, it’s possible that we’re only ever going to interact with it in one of those ways.
For example, you might only get the item out of it. We can communicate that intent to the compiler by adding the out variance annotation, like this:
 */
fun examine(boxed: Box<out Animal>) {
  val animal: Animal = boxed.item
  println(animal)
}

// The following will not compile. Because we declared the type argument as out, setters are removed from the projection
//fun insert(boxed: Box<out Animal>) {
//  boxed.item = Animal()
//}

fun <T: Animal> chooseFavorite1(pets: List<T>): T where T: Named {
  val favorite = pets[Random.nextInt(pets.size)]
  println("${favorite.name} is the favorite ${favorite.species}")
  return favorite
}

fun <T : Animal?> chooseFavorite(pets: List<T>): T {
  val favorite = pets[Random.nextInt(pets.size)]
  println("${favorite?.name ?: "Nobody"} is the favorite")
  return favorite
}
/*
Just remember that the ? goes on the type constraint, not the type parameter references. In other words, in this example, you wouldn’t specify List<T?> or return T?.

You’ll want to use a type parameter constraint when the following characteristics line up:
  * You want to invoke a particular function or property on the type.
  * You want to preserve the type when it’s returned.
Here’s a quick “cheat sheet” style table to help you decide what to use.
                              	  Need to invoke member                         	  No need to invoke member
Need to preserve the type     	  Use a generic with type parameter constraint  	  Use a generic without type parameter constraint
No need to preserve the type  	  Use a non-generic with proper abstraction     	  Go use a raw type in Java… ;)
*/

fun main() {
  // val x: List<Box<A>> = listOf(Box(A()), Box(null)) //doesn't compile
  val x: List<Box<out A?>> = listOf(Box(A()), Box(null)) //covariant
  examine(Box(Dog("tommy", "doberman")))
  val maybeCats: List<Cat?> = listOf(Cat("Whiskers"), null, Cat("Rosie"))
  val favorite: Cat? = chooseFavorite(maybeCats)
  println(favorite)
}