package org.njk.trials.typing.types

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.kotyle.kylix.either.Either

val mapper = ObjectMapper().registerModule(KotlinModule())

data class Fault(val message: String)
data class Fruit(val name: String)
data class Person(val fname: String, val lname: String)

interface ItemGetter {
    fun <T: Any> getList(data: String, fn: (String) -> List<T>): Either<Fault, List<T>>
}

class Getter : ItemGetter {
    override fun <T: Any> getList(data: String, fn: (String) -> List<T>): Either<Fault, List<T>> =
        try {
            Either.Right<Fault, List<T>>(deserializeJson(data, fn))
        } catch(e: Exception) {
            Either.Left<Fault, List<T>>(Fault("err-parsing-json"))
        }
}

fun <T: Any> deserializeJson(data: String, fn: (String) -> List<T>): List<T> = fn(data)

fun getPersons(personData: String): List<Person> = mapper.readValue(personData, mapper.typeFactory.constructCollectionType(List::class.java, Person::class.java))
fun getFruits(fruitsData: String): List<Fruit> = mapper.readValue(fruitsData, mapper.typeFactory.constructCollectionType(List::class.java, Fruit::class.java))

fun main(args: Array<String>) {
    val getter = Getter()
    val people = """[{"fname":"Naresh", "lname":"Khalasi"}]"""
    println(getter.getList(people, ::getPersons))
    println(getter.getList<Person>(people, {
        mapper.readValue(it, mapper.typeFactory.constructCollectionType(List::class.java, Person::class.java))
    }))
    println("-----------------------------------------------------")
    val fruits = """[{"name":"Mango"}, {"name":"Banana"}]"""
    println(getter.getList(fruits, ::getFruits))
    println(getter.getList(fruits, {
        mapper.readValue<List<Fruit>>(it, mapper.typeFactory.constructCollectionType(List::class.java, Fruit::class.java))
    }))
    println("------------------------------------------------------")
    val errData = """{"fname": "Naresh", "lname": "Khalasi"}"""
    println(getter.getList(errData, ::getPersons))
}