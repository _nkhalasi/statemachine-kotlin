package org.njk.trials.typing.lru

import java.util.*
import java.util.function.Function

open class LRUMap<K, V>(val maxSize: Int, val fn: ((K) -> V)?): LinkedHashMap<K, V>(16, 0.75F, true) {
  override fun removeEldestEntry(eldest: MutableMap.MutableEntry<K, V>?) = size > maxSize
  override fun get(key: K): V? = if (fn != null) super.computeIfAbsent(key,Function(fn)) else super.get(key)
}

fun <K,V> Collection<Pair<K,V>>.toLruMap(maxSize: Int=50, fn: ((K) -> V)?=null, sync: Boolean = false):  Map<K,V> =
    this.fold(LRUMap<K,V>(maxSize,fn)) { acc, elem ->
      acc.apply { put(elem.first, elem.second) }
    }.let { if (sync) Collections.synchronizedMap(it) else it}

fun main() {
  val names = mapOf("khalasi" to listOf("naresh", "krishang"),
      "patel" to listOf("Ramesh", "Kalpesh"), "chotara" to listOf("Samir"),
      "suthar" to listOf("Jatin"), "soni" to listOf("ketan"), "majmudar" to listOf("hemal"),
      "thakkar" to listOf("Hemal", "Suresh"))
  val lookup: (String) -> List<String>? = { n ->
    println("lookup invoked")
    names[n]
  }
  val cache = LRUMap(3, lookup)
  println("soni: ${cache["soni"]}")
  println("khalasi: ${cache["khalasi"]}")
  println("vani: ${cache["vani"]}")
  println("patel: ${cache["patel"]}")
  println("khalasi: ${cache["khalasi"]}")
  println("soni: ${cache["soni"]}")
  println("thakkar: ${cache["thakkar"]}")
  println(cache)
}
