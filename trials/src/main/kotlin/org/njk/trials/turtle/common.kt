package org.njk.trials.turtle

import java.time.LocalDateTime
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.round
import kotlin.math.sin

sealed class Measure(val s: String) {
  override fun toString() = s
}
class Kilometers : Measure("kms")
class Degrees : Measure("deg")
class Radians : Measure("rad")

data class FloatWithUnit<U>(val value: Float, val unit: U)

typealias Distance = FloatWithUnit<Kilometers>
typealias AngleInDegrees = FloatWithUnit<Degrees>
typealias AngleInRadians = FloatWithUnit<Radians>
fun AngleInDegrees.toRadians() =
    AngleInRadians((this@toRadians.value * (PI /180.0)).toFloat(), Radians())
fun Float.kms() = Distance(this@kms, Kilometers())
fun Float.deg() = AngleInDegrees(this@deg, Degrees())
fun Float.rad() = AngleInRadians(this@rad, Radians())

enum class Color(val rgb: Int) {
  RED(0xFF0000),
  GREEN(0x00FF00),
  BLUE(0x0000FF)
}

enum class PenState {
  UP, DOWN
}

data class Position(val x: Float, val y: Float)
fun Position.newPosition(d: Distance, a: AngleInDegrees) : Position =
    a.toRadians().let {
      val x0 = this@newPosition.x
      val y0 = this@newPosition.y

      val x1 = x0 + (d.value * cos(it.value))
      val y1 = y0 + (d.value * sin(it.value))
      Position(round(x1), round(y1))
    }

fun drawLine(oldPos: Position, newPos: Position, color: Color) =
    println("${LocalDateTime.now()} - Moved from $oldPos to $newPos and color is $color")

data class TurtleState(val position: Position, val angle: AngleInDegrees, val color: Color, val penstate: PenState)
