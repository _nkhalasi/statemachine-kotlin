package org.njk.trials.turtle.ooimmutable

import java.time.LocalDateTime
import org.njk.trials.turtle.*

class Turtle(val cs: TurtleState) {
  fun move(d: Distance): Turtle =
      Turtle(
          cs.copy(position = cs.position.newPosition(d, cs.angle))
      ).apply {
        if (this@apply.cs.penstate == PenState.DOWN) {
          drawLine(this@Turtle.cs.position, this@apply.cs.position, this@apply.cs.color)
        }
      }

  fun turn(a: AngleInDegrees): Turtle =
      Turtle(cs.copy(angle = ((cs.angle.value + a.value) % 360.0F).deg())
      ).apply {
        println("${LocalDateTime.now()} - Turn $a")
      }

  fun penUp(): Turtle =
      Turtle(cs.copy(penstate = PenState.UP)).apply {
        println("${LocalDateTime.now()} - Pen UP")
      }

  fun penDown(): Turtle =
      Turtle(cs.copy(penstate = PenState.DOWN)).apply {
        println("${LocalDateTime.now()} - Pen DOWN")
      }

  fun changeColor(newColor: Color): Turtle =
      Turtle(cs.copy(color = newColor)).apply {
        println("${LocalDateTime.now()} - Changed to $newColor")
      }

  companion object {
    fun initial() = Turtle(
        TurtleState(
            Position(0.0F, 0.0F),
            0.0F.deg(),
            Color.RED,
            PenState.DOWN
        )
    )
  }
}


fun drawTriangle() =
    Turtle.initial()
        .move(100.0F.kms())
        .turn(120.0F.deg())
        .move(100.0F.kms())
        .turn(120.0F.deg())
        .move(100.0F.kms())
        .turn(120.0F.deg())
        .let {
          println(it.cs)
        }

fun drawPolygon(sides: Int) {
  val angle = (180.0F - (360.0F/sides.toFloat())).deg()
  fun drawSide(t: Turtle, sideNumber: Int) =
      t.move(100.0F.kms()).turn(angle)

  (1..sides).fold(
      Turtle.initial()
  ) { t: Turtle, i: Int ->
    drawSide(t, 1)
  }
}

fun main(args: Array<String>) {
//  Turtle.initial()
//      .move(10.0F.kms())
//      .turn(90.0F.deg())
//      .move(10.0F.kms())
//      .penUp()
//      .changeColor(Color.BLUE)
//      .turn((-180.0F).deg())
//      .move(10.0F.kms())
//      .penDown()
//      .move(10.0F.kms())

  drawTriangle()
//  drawPolygon(6)
}
