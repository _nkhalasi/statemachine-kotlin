package org.njk.trials.turtle.ooapi

import org.njk.trials.turtle.*
import java.time.LocalDateTime

class Turtle(var cs: TurtleState) {
  fun move(d: Distance) {
    cs = cs.copy(position = cs.position.newPosition(d, cs.angle)).apply {
      if (cs.penstate == PenState.DOWN) {
        drawLine(cs.position, this@apply.position, this@apply.color)
      }
    }
  }

  fun turn(a: AngleInDegrees) {
    cs = cs.copy(angle = ((cs.angle.value + a.value) % 360.0F).deg()).apply {
      println("${LocalDateTime.now()} - Turn $a")
    }
  }

  fun penUp() {
    cs = cs.copy(penstate = PenState.UP).apply {
      println("${LocalDateTime.now()} - Pen UP")
    }
  }

  fun penDown() {
    cs = cs.copy(penstate = PenState.DOWN).apply {
      println("${LocalDateTime.now()} - Pen DOWN")
    }
  }

  fun changeColor(newColor: Color) {
    cs = cs.copy(color = newColor).apply {
      println("${LocalDateTime.now()} - Changed to $newColor")
    }
  }
  companion object {
    fun initial() = Turtle(
        TurtleState(
            Position(0.0F, 0.0F),
            0.0F.deg(),
            Color.RED,
            PenState.DOWN
        )
    )
  }
}

typealias TurtleApiException = Exception

fun validateDistance(dist: String) = try {
  dist.toFloat()
} catch(e: Exception) {
  throw TurtleApiException("Invalid distance '$dist' [${e.message}]")
}

fun validateAngle(ang: String) = try {
  ang.toFloat() * 1.0.toFloat()
} catch(e: Exception) {
  throw TurtleApiException("Invalid angle '$ang' [${e.message}]")
}

fun validateColor(color: String) = try {
  Color.valueOf(color)
} catch(e: Exception) {
  throw TurtleApiException("Invalid color '$color' [${e.message}]")
}

class TurtleApi(val turtle: Turtle) {
  fun exec(cmdString: String) = cmdString.split(" ")
      .map { it.trim() }.let {
        val param = it[1].toLowerCase()
        when (Pair(it[0].toLowerCase(), param)) {
          Pair("move", param) -> turtle.move(validateDistance(param).kms())
          Pair("turn", param) -> turtle.turn(validateAngle(param).deg())
          Pair("pen", "up") -> turtle.penUp()
          Pair("pen", "down") -> turtle.penDown()
          Pair("setcolor", param) -> turtle.changeColor(validateColor(param))
          else -> throw TurtleApiException("Instruction '$cmdString' is not recognized")
        }
      }
}

fun drawPolygon(sides: Int) {
  val angle = (180.0 - (360.0/sides.toFloat())).toFloat()
  val api = TurtleApi(Turtle.initial())

  fun drawOneSide() {
    api.exec("Move 100.0")
    api.exec("Turn $angle")
  }

  (0..sides).forEach { drawOneSide() }
}

fun triggerError() {
  val api = TurtleApi(Turtle.initial())
  api.exec("Move bad")
}

fun main(args: Array<String>) {
  drawPolygon(5)
  triggerError()
}
