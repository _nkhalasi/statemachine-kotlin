package org.njk.trials.turtle.oobasicfp

import org.njk.trials.turtle.*
import java.time.LocalDateTime

class Turtle {
  fun move(d: Distance) : (TurtleState) -> TurtleState =
      { cs ->
        cs.copy(position = cs.position.newPosition(d, cs.angle)).apply {
          if (cs.penstate == PenState.DOWN) {
            drawLine(cs.position, this@apply.position, this@apply.color)
          }
        }
      }

  fun turn(a: AngleInDegrees) : (TurtleState) -> TurtleState =
      { cs ->
          cs.copy(angle = ((cs.angle.value + a.value) % 360.0F).deg()).apply {
            println("${LocalDateTime.now()} - Turn $a")
          }
      }

  fun penUp() : (TurtleState) -> TurtleState =
      { cs ->
          cs.copy(penstate = PenState.UP).apply {
            println("${LocalDateTime.now()} - Pen UP")
          }
      }

  fun penDown() : (TurtleState) -> TurtleState =
      { cs ->
          cs.copy(penstate = PenState.DOWN).apply {
            println("${LocalDateTime.now()} - Pen DOWN")
          }
      }

  fun changeColor(newColor: Color) : (TurtleState) -> TurtleState =
      { cs ->
          cs.copy(color = newColor).apply {
            println("${LocalDateTime.now()} - Changed to $newColor")
          }
      }
}

val initialState = TurtleState(
    Position(0.0F, 0.0F),
    0.0F.deg(),
    Color.RED,
    PenState.DOWN
)

fun main(args: Array<String>) {
  Turtle().move(100F.kms())(initialState).let {
    println(it)
  }
}
