package org.njk.trials.turtle.fpdi

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import org.njk.trials.turtle.*
import java.io.Writer
import java.time.LocalDateTime

class Turtle {
  fun move(d: Distance) : (TurtleState) -> TurtleState =
      { cs ->
        cs.copy(position = cs.position.newPosition(d, cs.angle)).apply {
          if (cs.penstate == PenState.DOWN) {
            drawLine(cs.position, this@apply.position, this@apply.color)
          }
        }
      }

  fun turn(a: AngleInDegrees) : (TurtleState) -> TurtleState =
      { cs ->
        cs.copy(angle = ((cs.angle.value + a.value) % 360.0F).deg()).apply {
          println("${LocalDateTime.now()} - Turn $a")
        }
      }

  fun penUp() : (TurtleState) -> TurtleState =
      { cs ->
        cs.copy(penstate = PenState.UP).apply {
          println("${LocalDateTime.now()} - Pen UP")
        }
      }

  fun penDown() : (TurtleState) -> TurtleState =
      { cs ->
        cs.copy(penstate = PenState.DOWN).apply {
          println("${LocalDateTime.now()} - Pen DOWN")
        }
      }

  fun changeColor(newColor: Color) : (TurtleState) -> TurtleState =
      { cs ->
        cs.copy(color = newColor).apply {
          println("${LocalDateTime.now()} - Changed to $newColor")
        }
      }
}

val initialState = TurtleState(
    Position(0.0F, 0.0F),
    0.0F.deg(),
    Color.RED,
    PenState.DOWN
)

sealed class ErrorMessage
data class InvalidDistance(val msg: String) : ErrorMessage()
data class InvalidAngle(val msg: String) : ErrorMessage()
data class InvalidColor(val msg: String) : ErrorMessage()
data class InvalidCommand(val msg: String) : ErrorMessage()

fun validateDistance(dist: String): Either<ErrorMessage, Distance> =
  try {
    dist.toFloat().kms().right()
  } catch (e: Exception) {
    InvalidDistance("Invalid distance '$dist' [${e.message}]").left()
  }

//fun validateAngle(ang: String) = try {
//  Success(ang.toFloat() * 1.0.toFloat()).right()
//} catch(e: Exception) {
//  Failure(InvalidAngle("Invalid angle '$ang' [${e.message}]")).left()
//}
//
//fun validateColor(color: String) = try {
//  Success(Color.valueOf(color)).right()
//} catch(e: Exception) {
//  Failure(InvalidColor("Invalid color '$color' [${e.message}]")).left()
//}

//typealias ApiFunction = Function1<String, Either<ErrorMessage, TurtleState>>
typealias CmdString = String
typealias ApiFunction = (CmdString) -> Either<ErrorMessage, TurtleState>

//fun move(logger: Writer): ApiFunction = { cmdStr ->
//  validateDistance(cmdStr).flatMap {
//    val x = Turtle().move(initialState)
//    x
//  }
//}

