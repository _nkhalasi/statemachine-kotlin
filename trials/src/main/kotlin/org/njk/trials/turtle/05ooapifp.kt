package org.njk.trials.turtle.ooapifp

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import org.njk.trials.turtle.*
import java.time.LocalDateTime

class Turtle(val cs: TurtleState) {
  fun move(d: Distance): Turtle =
      Turtle(
          cs.copy(position = cs.position.newPosition(d, cs.angle))
      ).apply {
        if (this@apply.cs.penstate == PenState.DOWN) {
          drawLine(this@Turtle.cs.position, this@apply.cs.position, this@apply.cs.color)
        }
      }

  fun turn(a: AngleInDegrees): Turtle =
      Turtle(cs.copy(angle = ((cs.angle.value + a.value) % 360.0F).deg())
      ).apply {
        println("${LocalDateTime.now()} - Turn $a")
      }

  fun penUp(): Turtle =
      Turtle(cs.copy(penstate = PenState.UP)).apply {
        println("${LocalDateTime.now()} - Pen UP")
      }

  fun penDown(): Turtle =
      Turtle(cs.copy(penstate = PenState.DOWN)).apply {
        println("${LocalDateTime.now()} - Pen DOWN")
      }

  fun changeColor(newColor: Color): Turtle =
      Turtle(cs.copy(color = newColor)).apply {
        println("${LocalDateTime.now()} - Changed to $newColor")
      }

  companion object {
    fun initial() = TurtleState(
            Position(0.0F, 0.0F),
            0.0F.deg(),
            Color.RED,
            PenState.DOWN
        )
  }
}

sealed class ErrorMessage
data class InvalidDistance(val msg: String) : ErrorMessage()
data class InvalidAngle(val msg: String) : ErrorMessage()
data class InvalidColor(val msg: String) : ErrorMessage()
data class InvalidCommand(val msg: String) : ErrorMessage()

data class Success<T>(val value: T)
data class Failure<T: ErrorMessage>(val value: T)

fun validateDistance(dist: String) = try {
  Success(dist.toFloat()).right()
} catch(e: Exception) {
  Failure(InvalidDistance("Invalid distance '$dist' [${e.message}]")).left()
}

fun validateAngle(ang: String) = try {
  Success(ang.toFloat() * 1.0.toFloat()).right()
} catch(e: Exception) {
  Failure(InvalidAngle("Invalid angle '$ang' [${e.message}]")).left()
}

fun validateColor(color: String) = try {
  Success(Color.valueOf(color)).right()
} catch(e: Exception) {
  Failure(InvalidColor("Invalid color '$color' [${e.message}]")).left()
}

class TurtleApi(var ts: TurtleState) {
  fun updateState(ns: TurtleState) {
    ts = ns
  }

  fun exec(cmdString: String) = cmdString.split(" ")
      .map { it.trim() }.let {
        val param = it[1].toLowerCase()
        when (Pair(it[0].toLowerCase(), param)) {
          Pair("move", param) -> validateDistance(param).flatMap {
            val x = Turtle(ts).move(it.value.kms())
            updateState(x.cs).right()
          }
          Pair("turn", param) -> validateAngle(param).flatMap {
            val x = Turtle(ts).turn(it.value.deg())
            updateState(x.cs).right()
          }
          Pair("pen", "up") -> {
            val x = Turtle(ts).penUp()
            updateState(x.cs).right()
          }
          Pair("pen", "down") -> {
            val x = Turtle(ts).penDown()
            updateState(x.cs).right()
          }
          Pair("setcolor", param) -> validateColor(param).flatMap {
            val x = Turtle(ts).changeColor(it.value)
            updateState(x.cs).right()
          }
          else -> Failure(InvalidCommand("Instruction '$cmdString' is not recognized")).left()
        }
      }
}

fun drawPolygon(sides: Int) {
  val angle = (180.0 - (360.0/sides.toFloat())).toFloat()
  val api = TurtleApi(Turtle.initial())

  fun drawOneSide() {
    api.exec("Move 100.0")
    api.exec("Turn $angle")
  }

  (0..sides).forEach { drawOneSide() }
}

fun triggerError(sides: Int): List<Either<Failure<out ErrorMessage>, Unit>> {
  val angle = (180.0 - (360.0/sides.toFloat())).toFloat()
  val api = TurtleApi(Turtle.initial())

  fun drawASide(side: Int): Either<Failure<out ErrorMessage>, Unit> {
    api.exec("Move 100.0")
    return if (side > 3)
      api.exec("Turn bad")
    else
      api.exec("Turn $angle")
  }

  return (0..sides).map { drawASide(it) }.filter { it.isLeft() }
}

fun main(args: Array<String>) {
  drawPolygon(5)
  println("------------------------------------")
  println(triggerError(5))
}
