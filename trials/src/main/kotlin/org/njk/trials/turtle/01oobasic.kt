package org.njk.trials.turtle.oobasic

import org.njk.trials.turtle.*
import java.time.LocalDateTime

class Turtle(var cs: TurtleState) {
  fun move(d: Distance) {
    cs = cs.copy(position = cs.position.newPosition(d, cs.angle)).apply {
      if (cs.penstate == PenState.DOWN) {
        drawLine(cs.position, this@apply.position, this@apply.color)
      }
    }
  }

  fun turn(a: AngleInDegrees) {
    cs = cs.copy(angle = ((cs.angle.value + a.value) % 360.0F).deg()).apply {
      println("${LocalDateTime.now()} - Turn $a")
    }
  }

  fun penUp() {
    cs = cs.copy(penstate = PenState.UP).apply {
      println("${LocalDateTime.now()} - Pen UP")
    }
  }

  fun penDown() {
    cs = cs.copy(penstate = PenState.DOWN).apply {
      println("${LocalDateTime.now()} - Pen DOWN")
    }
  }

  fun changeColor(newColor: Color) {
    cs = cs.copy(color = newColor).apply {
      println("${LocalDateTime.now()} - Changed to $newColor")
    }
  }
}

val initialState = TurtleState(
    Position(0.0F, 0.0F),
    0.0F.deg(),
    Color.RED,
    PenState.DOWN
)

fun main(args: Array<String>) {
  val turtle = Turtle(initialState)
  println(turtle.cs)
  turtle.move(100F.kms())
  turtle.turn(120F.deg())
  println(turtle.cs)
}
