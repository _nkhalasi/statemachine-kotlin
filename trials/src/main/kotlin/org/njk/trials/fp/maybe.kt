package org.njk.trials.fp

sealed class Maybe<out T> {
    object None: Maybe<Nothing>()
    class Just<T>(val t: T): Maybe<T>()
}

fun main(args: Array<String>) {
    val j = Maybe.Just(1)
    val i = j.t
}