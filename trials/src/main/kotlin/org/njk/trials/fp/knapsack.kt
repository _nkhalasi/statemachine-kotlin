package org.njk.trials.fp


data class Item(val name: String, val weight: Int, val value: Double)

data class Knapsack(val value: Double, val items: MutableList<Item>, val weight: Int, val available: Int) {
    private fun add(item: Item) = Knapsack(value + item.value, (items + item) as MutableList<Item>,
                                    weight + item.weight, available - item.weight)

    private fun canAccept(item: Item) = item.weight <= available

    private fun maxValue(other: Knapsack) = if (this.value >= other.value) this else other

    override fun toString(): String {
        val iStr = items.foldRight("", {i, s -> "\t${i}\n${s}"})
        return "Total value: ${value}\nTotal weight: ${weight}\nItems: ${iStr}"
    }

    companion object {
        fun empty(capacity: Int): Knapsack = Knapsack(0.0, mutableListOf<Item>(), 0, capacity)
        fun pack(items: MutableList<Item>, knapsack: Knapsack): Knapsack = TODO()
        fun pack(items: MutableList<Item>, capacity: Int): Knapsack = TODO()
    }
}