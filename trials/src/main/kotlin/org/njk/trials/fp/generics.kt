package org.njk.trials.fp

import java.math.BigDecimal

fun Number.toBigDecimal(): BigDecimal = BigDecimal.valueOf(this.toDouble())
operator fun BigDecimal.plus(a: BigDecimal): BigDecimal = this.add(a)
operator fun BigDecimal.minus(a: BigDecimal): BigDecimal = this.subtract(a)
operator fun BigDecimal.times(a: BigDecimal): BigDecimal = this.multiply(a)
operator fun BigDecimal.div(a: BigDecimal): BigDecimal = this.divide(a)

class Box<T:Number>(val t: T) {
    fun <U:Number> inspect(u: U) = println("T: ${t.javaClass.name} and U: ${u.javaClass.name}")

    fun <U:Number> add(u: U): BigDecimal = t.toBigDecimal() + u.toBigDecimal()

    companion object {
        @JvmStatic fun main(args: Array<String>) {
            val intBox = Box<Int>(10)
            intBox.inspect(10.01)
//            intBox.inspect("some text")  //Won't compile
//            val strBox = Box<String>("Hello World")  //Won't compile

            println(intBox.add(10))
            println(intBox.add(10.01))
            println(intBox.add(10L))
        }
    }
}