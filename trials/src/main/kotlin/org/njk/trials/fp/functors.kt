package org.njk.trials.fp

interface Functor<T: Any> {
    fun <R: Any> map(mapper: (T) -> R): Functor<R>
}

class FunctorImpl<T : Any>(val t: T) : Functor<T> {
    override fun <R: Any> map(mapper: (T) -> R): Functor<R> = FunctorImpl(mapper(t))
}

val identity = { x: Any -> x }
val str = { x: Any -> x.toString() }

fun main(args: Array<String>) {
    val intIdentity: FunctorImpl<Int> = FunctorImpl<Int>(12)
    println(intIdentity.map(identity))
    println(intIdentity.map(str))
}

