package org.njk.trials.fp

import org.kotyle.kylix.either.Either
import org.kotyle.kylix.either.flatMap

data class Failure(val msg: String)
data class Success1(val n: Int)
data class Success2(val n: Int)
data class FinalSuccess(val n: Int)

fun step1(n: Int): Either<Failure, Success1> =
        if (n % 2 == 0) Either.Right<Failure,Success1>(Success1(n/2))
        else Either.Left<Failure,Success1>(Failure("step1"))

fun step2(s: Success1): Either<Failure, Success2> =
        if (s.n % 2 == 0) Either.Right<Failure,Success2>(Success2(s.n/2))
        else Either.Left<Failure,Success2>(Failure("step2"))

fun step3(s: Success2): Either<Failure, FinalSuccess> =
        if (s.n % 2 == 0) Either.Right<Failure,FinalSuccess>(FinalSuccess(s.n/2))
        else Either.Left<Failure,FinalSuccess>(Failure("step3"))

fun foo(n: Int): Either<Failure, FinalSuccess> =
        step1(n).right().flatMap { step2(it) }.right().flatMap { step3(it) }

fun main(args: Array<String>) {
    (1..8).forEach {
        println("Returned value for ${it} is ${foo(it)}")
    }
}
