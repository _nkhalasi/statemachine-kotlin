package org.njk.trials.fp

import java.util.*

data class Address(val houseNo: String, val building: String, val street: Optional<String>, val city: Optional<String>, val state: Optional<String>)
data class Person(val firstName: String, val middleName: Optional<String>, val lastName: String, val address: Optional<Address>)

fun main(args: Array<String>) {
    val persons =  mapOf(
        "Naresh" to Person("Naresh", Optional.of("J"), "Khalasi", Optional.empty()),
        "Krishang" to Person("Krishang", Optional.empty(), "Khalasi", Optional.of(Address("189", "San Tropez", Optional.empty(), Optional.empty(), Optional.empty())))
    )

//    persons.get("Naresh").
}