package org.njk.trials.football

/**
 * This is an exercise we have been giving to some interview candidates. Attached is a file containing football team scores.
 * Your mission, ... if you choose to accept it is
 * 1. Read file as is
 * 2. Split it into 4 different blocks of teams grouped by based on total points of the team (highest 5 first, next 5 .. etc)
 * 3. For each block compute the sum of the points of all the teams in the block. Also compute the average of the
 * difference between For and Away goals. Print these two values
 */

typealias Team = String
typealias MatchesPlayed = Int
typealias MatchesWon = Int
typealias MatchesLost = Int
typealias MatchesDraw = Int
typealias ForGoals = Int
typealias AwayGoals = Int
typealias Points = Int
data class Score(val teamName: Team, val played: MatchesPlayed, val won: MatchesWon, val lost: MatchesLost,
                 val draw: MatchesDraw, val forGoals: ForGoals, val awayGoals: AwayGoals, val points: Points)

fun loadData() =
    Score::class.java.getResource("/football.dat").let { f ->
        f.openStream().bufferedReader().useLines { lines ->
            lines.map { it.substring(7) }
                .filter { !it.startsWith("-") && !it.startsWith("Team") }
                .map {
                    val fields = it.split(" ").filter { !it.equals("") && !it.equals("-") }
                    Score(fields[0], fields[1].toInt(), fields[2].toInt(), fields[3].toInt(), fields[4].toInt(),
                        fields[5].toInt(), fields[6].toInt(), fields[7].toInt())
                }
                .sortedWith(Comparator { o1, o2 -> if (o1.points < o2.points) 1 else if (o1.points > o2.points) -1 else 0 })
                .toList()
        }
    }

fun main(args: Array<String>) {
    val dataLines = loadData()
    val buckets = dataLines.size
    dataLines.forEach {
        println(it)
    }
}
