package org.njk.trials

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.KeyStore
import java.security.MessageDigest
import java.security.cert.X509Certificate
import javax.net.ssl.*

class SavingTrustManager(val tm: X509TrustManager) : X509TrustManager {
  private var certChain: Array<X509Certificate>? = null
  override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
    this.certChain = chain
    tm.checkServerTrusted(chain, authType)
  }

  override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String): Unit {}
  override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
  val chain: Array<X509Certificate>?
    get() = this.certChain
}

const val HEXDIGITS = "0123456789abcdef"

fun toHexString(bytes: ByteArray): String {
  val outhex = StringBuilder(bytes.size * 3)
  val hexdigits = HEXDIGITS.toCharArray()
  bytes.forEach {
    val b = it.toInt() and 0xff
    outhex.append(hexdigits[b shr 4])
    outhex.append(hexdigits[b and 15])
    outhex.append(' ')
  }
  return outhex.toString()
}

fun loadSystemTrustStore() = KeyStore.getInstance(KeyStore.getDefaultType()).apply {
  val trustStoreLocation = System.getProperties().getProperty("java.home") + File.separator + "lib" + File.separator + "security" + File.separator + "cacerts"
  println(trustStoreLocation)
  load(
      FileInputStream(trustStoreLocation), "changeit".toCharArray()
  )
}
fun loadKeyStore(passphrase: String) = KeyStore.getInstance(KeyStore.getDefaultType()).apply { load(null, passphrase.toCharArray()) }
fun initTrustManagerFactory(ks: KeyStore) = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply { init(ks) }
fun initSslSocketFactory(tms: Array<TrustManager>) = SSLContext.getInstance("TLS").apply { init(null, tms, null) }.socketFactory

fun fetchServerCertificate(host: String, port: Int, passphrase: String): Array<X509Certificate>? {
  val tm = SavingTrustManager(initTrustManagerFactory(loadSystemTrustStore()).trustManagers[0] as X509TrustManager)
  initSslSocketFactory(arrayOf(tm)).let { it.createSocket(host, port) as SSLSocket }.apply {
    soTimeout = 10000
    println("Starting SSL handshake...")
    try {
      startHandshake()
      close()
      println()
      println("No Errors, certificate is already trusted")
    } catch (e: SSLException) {
      println()
      e.printStackTrace()
    }
  }
  return tm.chain
}

fun dumpCertificates(chain: Array<X509Certificate>?) {
  println()
  println("Server sent ${chain!!.size} certificate(s):")
  println()
  val sha1 = MessageDigest.getInstance("SHA1")
  val md5 = MessageDigest.getInstance("MD5")
  chain.forEach {
    println(" Subject ${it.subjectDN}")
    println(" Issuer  ${it.issuerDN}")
    sha1.update(it.encoded)
    println(" sha1    ${toHexString(sha1.digest())}")
    md5.update(it.encoded)
    println(" md5     ${toHexString(md5.digest())}")
    println()
  }
}

fun getCertificateToTrust(chain: Array<X509Certificate>?): X509Certificate {
  val reader = System.`in`.bufferedReader()
  println("Enter certificate to add to trusted keystore or 'q' to quit: [1]")
  val inp = reader.readLine().trim()
  val idx = when {
    (inp.length == 0) -> 0
    else -> (inp.toInt() - 1)
  }
  return chain!![idx]
}

fun createCertStore(cert: X509Certificate, alias: String, passphrase: String) {
  FileOutputStream("abcxyz").use { out ->
    loadKeyStore(passphrase).let {
      it.setCertificateEntry(alias, cert)
      it.store(out, passphrase.toCharArray())
    }
  }
}

fun main(args: Array<String>) {
  val host = args[0]
  val port = args[1].toInt()
  val passphrase = args[2]

  val chain = fetchServerCertificate(host, port, passphrase)
  dumpCertificates(chain)
  val cert = getCertificateToTrust(chain)
  createCertStore(cert, host, passphrase)
}

