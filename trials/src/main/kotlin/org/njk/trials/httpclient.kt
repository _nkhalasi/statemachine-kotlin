package org.njk.trials

import org.apache.commons.logging.LogFactory
import org.apache.http.HttpEntityEnclosingRequest
import org.apache.http.client.HttpRequestRetryHandler
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import java.io.InterruptedIOException
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.*
import javax.net.ssl.SSLException

val LOG = LogFactory.getLog("org.njk.trials.httpclient")

val retryHandler = HttpRequestRetryHandler { ex, ec, ctx ->
  println("Try request: $ec")

  if (ec >= 5) return@HttpRequestRetryHandler false
  val isRetryableException = when (ex) {
    is InterruptedIOException -> false
    is UnknownHostException -> false
    is SSLException -> false
//    is ConnectException -> false
    else -> true
  }
  if (isRetryableException) {
    println("Did it come here?")
    val clientCtx = HttpClientContext.adapt(ctx)
    val request = clientCtx.request
    when (request) {
      is HttpEntityEnclosingRequest -> false
      else -> true //idempotent request
    }
  } else
    false
}

fun main(args: Array<String>) {

  HttpClientBuilder.create()
//      .setRetryHandler(retryHandler)
      .setRetryHandler(DefaultHttpRequestRetryHandler(5, true))
      .setDefaultRequestConfig(
          RequestConfig.custom()
              .setConnectTimeout(2 * 1000)
              .setConnectionRequestTimeout(2 * 1000)
              .setSocketTimeout(6 * 1000)
              .build()
      )
      .build().use { httpClient ->
//        val httpGet = HttpGet("http://localhost:1234")
        val httpGet = HttpGet("http://www.fakeresponse.com/api/?sleep=450")
        println("Executing request ${httpGet.requestLine}")
        httpClient.execute(httpGet).use {
          println(it)
          EntityUtils.consume(it.entity)
        }
        println("Request finished")
      }
}
