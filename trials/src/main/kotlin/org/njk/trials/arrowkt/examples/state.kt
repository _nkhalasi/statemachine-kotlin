package org.njk.trials.arrowkt.examples

import arrow.core.toT
import arrow.data.State
import arrow.data.StateApi
import arrow.data.run


fun main(args: Array<String>) {
  val incr = State { n: Int -> n+1 toT n}
  println(incr.run(3))
  println(incr.run(4))

  val s1 = StateApi.just<String, Int>(1)
  println(s1.run("foo"))

  val s2 = StateApi.get<String>()
  println(s2.run("foo"))

  val s3 = StateApi.modify<String> { "bar" }
  println(s3.run("foo"))

  val s4 = StateApi.set("bar")
  println(s4.run("foo"))
}