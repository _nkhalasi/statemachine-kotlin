package org.njk.trials.arrowkt.examples

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.data.NonEmptyList
import arrow.data.Validated
import arrow.data.invalid
import arrow.data.valid
import java.sql.Connection

data class ConnectionParams(val url: String, val port: Int)

abstract class Read<A> {
  abstract fun read(s: String): Option<A>
  companion object {
    val stringRead: Read<String> =
        object: Read<String>() {
          override fun read(s: String): Option<String> = Option(s)
        }
    val intRead: Read<Int> =
        object: Read<Int>() {
          override fun read(s: String): Option<Int> =
              if (s.matches(Regex("-?[0-9]+"))) Option(s.toInt()) else None
        }
  }
}

sealed class ConfigError {
  data class MissingConfig(val field: String): ConfigError()
  data class ParseConfig(val field: String): ConfigError()
}

data class Config(val map: Map<String, String>) {
  fun <A> parse(read: Read<A>, key: String): Validated<ConfigError, A> {
    val v = Option.fromNullable(map[key])
    return when (v) {
      is Some -> {
        val s = read.read(v.t)
        when (s) {
          is Some -> s.t.valid()
          is None -> ConfigError.ParseConfig(key).invalid()
        }
      }
      is None -> Validated.Invalid(ConfigError.MissingConfig(key))
    }
  }
}

fun <E, A, B, C> parallelValidate(v1: Validated<E, A>,
                                  v2: Validated<E, B>,
                                  f: (A, B) -> C): Validated<NonEmptyList<E>, C> {
  return when {
    v1 is Validated.Valid && v2 is Validated.Valid -> Validated.Valid(f(v1.a, v2.a))
    v1 is Validated.Valid && v2 is Validated.Invalid -> v2.toValidatedNel()
    v1 is Validated.Invalid && v2 is Validated.Valid -> v1.toValidatedNel()
    v1 is Validated.Invalid && v2 is Validated.Invalid -> Validated.Invalid(NonEmptyList(v1.e, listOf(v2.e)))
    else -> throw IllegalStateException("Not possible value")
  }
}

fun main(args: Array<String>) {
  val c1 = Config(mapOf("url" to "127.0.0.1", "port" to "3377"))

  val c1v = parallelValidate(
      c1.parse(Read.stringRead, "url"),
      c1.parse(Read.intRead, "port")
  ) { url, port ->
    ConnectionParams(url, port)
  }

  println(c1v)
  println("-------------------------------")


  val c2 = Config(mapOf("url" to "127.0.0.1", "port" to "abcd"))

  val c2v = parallelValidate(
      c2.parse(Read.stringRead, "url"),
      c2.parse(Read.intRead, "port")
  ) { url, port ->
    ConnectionParams(url, port)
  }

  println(c2v)
  println("-------------------------------")


  val c3 = Config(mapOf("port" to "3377"))

  val c3v = parallelValidate(
      c3.parse(Read.stringRead, "url"),
      c3.parse(Read.intRead, "port")
  ) { url, port ->
    ConnectionParams(url, port)
  }

  println(c3v)
  println("-------------------------------")


  val c4 = Config(mapOf("port" to "abcd"))

  val c4v = parallelValidate(
      c4.parse(Read.stringRead, "url"),
      c4.parse(Read.intRead, "port")
  ) { url, port ->
    ConnectionParams(url, port)
  }

  println(c4v)
  println("-------------------------------")

}