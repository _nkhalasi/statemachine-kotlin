package org.njk.trials.arrowkt.examples

import arrow.data.*

fun main(args: Array<String>) {

  val r = { a: Int -> a * 2 }.reader()
  val y = r.map {
    println (it)
    it * 3
  }.runId(3)
  println(y)

  val r1 = { x: String -> "Hello $x" }.reader().map { "Welcome $it" }.runId("Naresh")
  println(r1)

  val r2 = { x: String -> "Hello $x" }.reader()
      .flatMap { a -> Reader().just<String, String>("Welcome $a") }
      .runId("Naresh")
  println(r2)
}
