package org.njk.trials.arrowkt.examples

import arrow.core.None
import arrow.core.Some
import arrow.core.or


fun main(args: Array<String>) {
  val naresh = Some("Naresh")
  println(naresh)
  val y = naresh.flatMap {
    Some("Hello $it")
  }
  println(y)
  println(y.toList())

  val none = None
  println (naresh or none)
  println(none or naresh)
  println(naresh and none)
  println(none and naresh)

  println(naresh.map { "Hello $it" })
}