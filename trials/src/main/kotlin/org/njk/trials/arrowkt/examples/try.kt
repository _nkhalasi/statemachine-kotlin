package org.njk.trials.arrowkt.examples

import arrow.core.*
import java.io.File

object Nuke
object Target
object Impacted

fun arm(): Try<Nuke> = Try { throw Exception("System-Offline") }
fun armSuccess(): Try<Nuke> = Try { Nuke }
fun aim(): Try<Target> = Try { throw Exception("Rotation-needs-Oil") }
fun launch(target: Target, nuke: Nuke): Try<Impacted> = Try { throw Exception("missed-by-meters") }

fun main(args: Array<String>) {
  val result = arm()
  println(result)
  println(
      result.fold(
          { ex -> "Booooom!: $ex" },
          { "Got!: $it" }
      )
  )
  println(
      result.flatMap {
        Try { it }
      }
  )
  println(result.isSuccess())
  println(result.isFailure())
  println(result.failed())

  println("------------------------------")
  val result1 = armSuccess()
  println(result1)
  println(result1.isSuccess())
  println(result1.isFailure())
  println(result1.failed())

  println("------------------------------")
  val f = File("/home/nkhalasi/Downloads/tmp/abc.txt")
  val x = Try {
    f.bufferedReader().use {
      it.readLine()
    }
  }
  when (x) {
    is Success -> println("S u c c e s s")
    is Failure -> println("F a i l u r e")
  }
  println(x)
  println(x.toEither())
  println(x.getOrElse { "Failed to read file" })
  println(x.getOrDefault { "Default Name" })
  println(x.flatMap {
    Try.just(it)
  })
}

