package org.njk.trials.arrowkt.examples

import arrow.core.Either
import arrow.core.flatMap
import arrow.instances.either.monad.monad

fun main(args: Array<String>) {
  val r: Either<String, Int> = Either.Right(5)
  val l: Either<String, Int> = Either.Left("error")

  println(r.map {
    Either.Right(it + 1)
  })
  println(r.flatMap {
    Either.Right(it + 5)
  })

  val a = Either.monad<Int>().binding {
    val a = Either.Right(1).bind()
    val b = Either.Right(1 + a).bind()
    val c = Either.Right(1 + b).bind()
    a+b+c
  }
  println(a)

//  Either.functor<Int>().map(Either.Right(1), { it+1 })
//  println(Either.functor<Int>())
}