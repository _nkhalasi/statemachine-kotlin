package org.njk.trials

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime

fun main(args: Array<String>) {
  val srcZone = ZoneId.of("Asia/Kolkata")
  val targetZone = ZoneId.of("UTC")

//  val srcEpochTime = 1524767400L
  val srcEpochTime = 1524660463L
  val srcEpochInstant = Instant.ofEpochSecond(srcEpochTime)
  val srcEpochInstantAtZone = srcEpochInstant.atZone(srcZone)

  val now = Instant.now()
  println(now)
  println(now.epochSecond)
  println(now.atZone(srcZone))
  println(now.atZone(targetZone))
  println(now.minusSeconds( 120 * 60).atZone(targetZone))
  println(Instant.ofEpochMilli(srcEpochTime))
  println("-----")
  println(srcEpochInstant)
  println(srcEpochInstantAtZone)
  println(srcEpochInstantAtZone.toInstant())
  println(ZonedDateTime.ofInstant(srcEpochInstant, targetZone))
}