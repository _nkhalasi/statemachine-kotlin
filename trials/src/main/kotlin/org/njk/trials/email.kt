package org.njk.trials

import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


class SmtpConfig(val host:String, val port:Int, val auth:Boolean, val enableTls:Boolean, val debug:Boolean=false): Properties() {
    init {
        put("mail.debug", debug.toString())
        put("mail.smtp.auth", auth.toString())
        put("mail.smtp.starttls.enable", enableTls.toString())
        put("mail.smtp.host", host)
        put("mail.smtp.port", port.toString())
    }
}

fun Message.from(addr: InternetAddress) = addFrom(arrayOf(addr))
fun Message.to(vararg addrs: InternetAddress) = addRecipients(Message.RecipientType.TO, addrs)
fun Message.replyTo(addr: InternetAddress) = setReplyTo(arrayOf(addr))
fun Message.mailSubject(subject: String) = setSubject(subject)
fun Message.mailBody(body: String) = setText(body)

fun mailer(config: Properties, username: String, password: String): (InternetAddress, InternetAddress, String, String) -> Unit {
    val sendMail = { from: InternetAddress, to: InternetAddress, subject: String, body: String -> Unit
        val session = Session.getDefaultInstance(config,
            object: Authenticator() {
                override fun getPasswordAuthentication(): PasswordAuthentication = PasswordAuthentication(username, password)
            })
        val message = MimeMessage(session).apply {
            from(from)
            to(to)
            mailSubject(subject)
            mailBody(body)
        }
        Transport.send(message)
    }
    return sendMail
}


fun main(args: Array<String>) {
    val smtpConfig = SmtpConfig("smtp.gmail.com", 587, true, true, true)
    val username = "testmailsender@vayana.in"
    val password = "xxxxx"

    val mailSender = mailer(smtpConfig, username, password)
    mailSender(InternetAddress("khalasi@gmail.com"),
        InternetAddress("naresh@vayana.in"),
        "Testing Subject",
        "Dear Mail Crawler, \n\n No spam to my email, please!")
    println("Done!!!")
}
