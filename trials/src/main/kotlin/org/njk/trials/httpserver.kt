package org.njk.trials

import com.sun.net.httpserver.HttpExchange
import java.net.InetSocketAddress
import java.util.concurrent.Executors
import kotlin.concurrent.thread

fun main(args: Array<String>) {
    HttpServer("localhost", 8000).start()
}


class HttpServer(val host: String, val port: Int) {
    fun start() {
        val addr = InetSocketAddress(host, port)
        val server = com.sun.net.httpserver.HttpServer.create(addr, 0)
        server.apply {
            createContext("/reljio/auth", ReverseHandler())
            executor = Executors.newCachedThreadPool()
            println("Press CTRL+C to stop...")
            start()
            Runtime.getRuntime().addShutdownHook(thread( start=false,
                                                         block={println("Shutting down"); stop(0)} )
                                            )
        }
    }
}

class ReverseHandler : com.sun.net.httpserver.HttpHandler {
    override fun handle(httpExchange: HttpExchange) {
        val method = httpExchange.requestMethod
        when (method.toUpperCase()) {
            "GET" -> {
                val respHeaders = httpExchange.responseHeaders
                respHeaders.set("Content-Type", "text/plain")

                val query: String? = httpExchange.requestURI.rawQuery
                query?.let {
                    val paramStrings = it.split("&")
                    assert(paramStrings.size == 2)

                    httpExchange.sendResponseHeaders(200, 0)
                    val respBody = httpExchange.responseBody
                    respBody.use { b ->
                        val params = paramStrings.map { p -> p.split("=") }
                        params.forEach { p -> b.write("${p[0]} = ${p[1]}\n".toByteArray()) }
                    }
                }
                httpExchange.sendResponseHeaders(400, 0)
            }
            else -> {
                httpExchange.sendResponseHeaders(405, 0)
            }
        }
    }
}