package org.njk.trials.scribejava

import com.github.scribejava.apis.GoogleApi20
import com.github.scribejava.core.builder.ServiceBuilder
import java.util.*


fun main(args: Array<String>) {
    val NETWORK_NAME = "Google"

    val clientId = "1020196751800-tunhkfbnf97hamn3r8fohu0ebhaobt9n.apps.googleusercontent.com"
    val clientSecret = "04Q5RI95vCaDT6uyGUmVhBKz"
    val service = ServiceBuilder().apiKey(clientId)
        .apiSecret(clientSecret)
        .callback("urn:ietf:wg:oauth:2.0:oob")
        .scope("https://www.googleapis.com/auth/userinfo.email")
        .build(GoogleApi20.instance())

    val _in = Scanner(System.`in`, "UTF-8")

    println("=== $NETWORK_NAME's OAuth Workflow ===")
    println()

    println("Fetching the Authorization URL...")
    val authUrl = service.authorizationUrl
    println("Go to the Authorization URL!")
    println("Now go and authorize ScribeJava here:")
    println(authUrl)
    println("And paste the authorization code here")
    println(">>")
    val code = _in.nextLine()
    println()

    println("Trading the Request Token for an Access Token...")
    val accessToken = service.getAccessToken(code)
    println("Got the Access Token!")
    println("(if your curious it looks like this: $accessToken, 'rawResponse'=${accessToken.rawResponse})")
    println()
}


/*
=== Google's OAuth Workflow ===

Fetching the Authorization URL...
Go to the Authorization URL!
Now go and authorize ScribeJava here:
https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=1020196751800-tunhkfbnf97hamn3r8fohu0ebhaobt9n.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&state=secret967297
And paste the authorization code here
>>
4/GVjbvzPAbdhNZiZ-N56mWGWk5ak7KE_w5QebvrP7JNg

Trading the Request Token for an Access Token...
Got the Access Token!
(if your curious it looks like this: GoogleToken{access_token=ya29..tgKxu7WQikXuOhSU1Q_vjQJEfjLItWWB-0lJFQF_74m8EL2NOv7og-qjybY1VckDQw, token_type=Bearer, expires_in=3599, refresh_token=1/R70XXLoamECzkbToiijIN94J2_btonaZzGCL5WQx7Uc, scope=null, open_id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6IjgwODdkMjU4YWMxOWMwZmNmMWRhYjdhOTA4YzIyMWNkZDgxZDU1MTIifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhdF9oYXNoIjoidTJnX01vVDl2RnlmT2k3WG1IdFJadyIsImF1ZCI6IjEwMjAxOTY3NTE4MDAtdHVuaGtmYm5mOTdoYW1uM3I4Zm9odTBlYmhhb2J0OW4uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDQ4NTEzMTQ2OTk5NjU3MjQ0NzMiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiMTAyMDE5Njc1MTgwMC10dW5oa2ZibmY5N2hhbW4zcjhmb2h1MGViaGFvYnQ5bi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoia2hhbGFzaUBnbWFpbC5jb20iLCJpYXQiOjE0NTk0MjUyNDUsImV4cCI6MTQ1OTQyODg0NX0.oD7ooPET3pqafYMcn0a-R0IenxiIdwWtszG1QAa_0dzTEhtT8eB8QpfG4ie20xudAB9Muxvw_CxqxmVB58exUWnx6VQFcilq-6pZO_89wKFEeyAPNVS3P29huKlINzcBCFcUlrnmANzXPo9bE8YeR6NtfGyPffOdAilVZ1mSNW01r_tJn2mLOLZ5-VFD1Y-WZtM1oAINqQJVuc37F7L-c9WuDcySAv8S47dCQwk3ZKjLKFjYng16OY-yqV7N807e5urKxx7JsdvxrPLXQ6RNRZvcQNKYBJ2Fg_V9wtjugbxvcOpEow3c461eIuDnqZXMbZkFsWQNXzxyresM87-iIA}, 'rawResponse'={
 "access_token": "ya29..tgKxu7WQikXuOhSU1Q_vjQJEfjLItWWB-0lJFQF_74m8EL2NOv7og-qjybY1VckDQw",
 "token_type": "Bearer",
 "expires_in": 3599,
 "refresh_token": "1/R70XXLoamECzkbToiijIN94J2_btonaZzGCL5WQx7Uc",
 "id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjgwODdkMjU4YWMxOWMwZmNmMWRhYjdhOTA4YzIyMWNkZDgxZDU1MTIifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhdF9oYXNoIjoidTJnX01vVDl2RnlmT2k3WG1IdFJadyIsImF1ZCI6IjEwMjAxOTY3NTE4MDAtdHVuaGtmYm5mOTdoYW1uM3I4Zm9odTBlYmhhb2J0OW4uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDQ4NTEzMTQ2OTk5NjU3MjQ0NzMiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiMTAyMDE5Njc1MTgwMC10dW5oa2ZibmY5N2hhbW4zcjhmb2h1MGViaGFvYnQ5bi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoia2hhbGFzaUBnbWFpbC5jb20iLCJpYXQiOjE0NTk0MjUyNDUsImV4cCI6MTQ1OTQyODg0NX0.oD7ooPET3pqafYMcn0a-R0IenxiIdwWtszG1QAa_0dzTEhtT8eB8QpfG4ie20xudAB9Muxvw_CxqxmVB58exUWnx6VQFcilq-6pZO_89wKFEeyAPNVS3P29huKlINzcBCFcUlrnmANzXPo9bE8YeR6NtfGyPffOdAilVZ1mSNW01r_tJn2mLOLZ5-VFD1Y-WZtM1oAINqQJVuc37F7L-c9WuDcySAv8S47dCQwk3ZKjLKFjYng16OY-yqV7N807e5urKxx7JsdvxrPLXQ6RNRZvcQNKYBJ2Fg_V9wtjugbxvcOpEow3c461eIuDnqZXMbZkFsWQNXzxyresM87-iIA"
}
)


Process finished with exit code 0
 */