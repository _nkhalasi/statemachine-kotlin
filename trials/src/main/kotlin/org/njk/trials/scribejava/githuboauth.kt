package org.njk.trials.scribejava

import com.github.scribejava.apis.GitHubApi
import com.github.scribejava.core.builder.ServiceBuilder
import java.util.*


fun main(args: Array<String>) {
    val NETWORK_NAME = "GitHub"

    val redirectUrl = "http://localhost:8000/reljio/auth"
    val clientId = "9ce6203a80843b1be8d1"
    val clientSecret = "e3545e8cd85e2dd936abcd7677586de59cd4345d"
    val secretState = "secret" + Random().nextInt(999999)
    val service = ServiceBuilder().apiKey(clientId)
                                .apiSecret(clientSecret)
                                .state(secretState)
                                .callback(redirectUrl)
                                .build(GitHubApi.instance())
    val _in = Scanner(System.`in`, "UTF-8")

    println("=== $NETWORK_NAME's OAuth Workflow ===")
    println()

    println("Fetching the Authorization URL...")
    val authUrl = service.authorizationUrl
    println("Go to the Authorization URL!")
    println("Now go and authorize ScribeJava here:")
    println(authUrl)
    println("And paste the authorization code here")
    println(">>")
    val code = _in.nextLine()
    println()

    println("And paste the state from server here. We have set 'secretState'='" + secretState + "'.")
    println(">>")
    val value = _in.nextLine()

    if (secretState.equals(value))
        println("State value does match!")
    else {
        println("Ooops, state value does not match!")
        println("Expected = $secretState, Got = $value")
        println()
    }

    println("Trading the Request Token for an Access Token...")
    val accessToken = service.getAccessToken(code)
    println("Got the Access Token!")
    println("(if your curious it looks like this: $accessToken, 'rawResponse'=${accessToken.rawResponse})")
    println()
}


/*
=== GitHub's OAuth Workflow ===

Fetching the Authorization URL...
Go to the Authorization URL!
Now go and authorize ScribeJava here:
https://github.com/login/oauth/authorize?client_id=9ce6203a80843b1be8d1&redirect_uri=http%3A%2F%2Flocalhost%3A8000&state=secret971908
And paste the authorization code here
>>
825df9d0c771a14f7f9a

And paste the state from server here. We have set 'secretState'='secret971908'.
>>
secret971908
State value does match!
Trading the Request Token for an Access Token...
Got the Access Token!
(if your curious it looks like this: OAuth2AccessToken{access_token=7087ddd4fa26342d81495b64a2d1e0b75eaf5217, token_type=bearer, expires_in=null, refresh_token=null, scope=null}, 'rawResponse'=access_token=7087ddd4fa26342d81495b64a2d1e0b75eaf5217&scope=&token_type=bearer)


Process finished with exit code 0
 */