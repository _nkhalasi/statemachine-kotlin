package org.njk.trials.fptrng.readereither

import arrow.core.Either
import arrow.core.right
import arrow.data.*
import arrow.instances.monad
import org.joda.time.DateTime


/****************************/
//class ForReaderN<D> private constructor() { companion object }
//typealias ReaderNOf<D, A> = arrow.Kind<ForReaderN<D>, A>
//typealias ReaderNKindedJ<D, A> = io.kindedj.Hk<ForReaderN<D>, A>
//@Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")
//inline fun <D, A> ReaderNOf<D, A>.fix(): Reader<D, A> =
//    this as Reader<D, A>
/****************************/
//fun <T, D> Reader<D, Either<Fault,T>>.et(): EitherT<ForReaderT, Fault, T> = EitherT(this)

class ServiceOne {
  init {
    println("\tService one built at ${DateTime.now()}")
  }
  fun process() = println("\tService one invoked at ${DateTime.now()}")
}
class ServiceTwo {
  init {
    println("\tService two built at ${DateTime.now()}")
  }
  fun process() = println("\tService Two invoked at ${DateTime.now()}")
}

data class Fault(val message: String)
data class Config(val time: DateTime)

fun buildServiceOne(): Reader<Config, Either<Fault, ServiceOne>> = { c: Config ->
  ServiceOne().right()
}.reader()

fun buildServiceTwo(): Reader<Config, Either<Fault, ServiceTwo>> = { c: Config ->
  ServiceTwo().right()
}.reader()

fun buildServicesR1(c: Config) {
  println("buildServicesR1()")
  Reader().ask<Config>().flatMap{ buildServiceOne() }.flatMap{ buildServiceTwo() }.run(c)
}

fun buildServicesR2(c: Config) {
  println("buildServicesR2()")
  Reader().monad<Config>().binding {
    val s1 = buildServiceOne().bind()
    val s2 = buildServiceTwo().bind()
  }.fix().run(c)
}

fun buildServicesR3(c: Config) {
  println("buildServicesR3()")

  //Its not possible to combine Reader monad with another monad and run transformation since the underlying necessary
  //instance classes are not available yet

//  ForReaderT(Either.monad<Fault>()) extensions {
//
//  }
//
//  val a = Either.monad<Fault>()
//  val x = Reader().monad<Config>()
//  val y = ForEitherT<ReaderPartialOf<Config>, Fault>(x) extensions {
//    binding {
//      val s1 = buildServiceOne().bind<Either<Fault, ServiceOne>>()
//      val s2 = buildServiceTwo().bind<Either<Fault, ServiceTwo>>()
//    }.fix()
//  }
}

fun main(args: Array<String>) {
  val conf = Config(DateTime.now())
  buildServicesR1(conf)
  buildServicesR2(conf)
}
