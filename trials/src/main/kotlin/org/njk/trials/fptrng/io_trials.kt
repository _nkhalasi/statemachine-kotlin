package org.njk.trials.fptrng

import arrow.core.*
import arrow.effects.IO
import java.io.File
import arrow.effects.instances.io.monad.binding as ioBinding

fun ioAttempt() {
  val xEt: Either<Throwable, Int> = IO<Int> { throw RuntimeException("Boom!") }.attempt().unsafeRunSync()
  println(xEt)
  val xI: Int = IO<Int> { throw RuntimeException("Boom!") }.unsafeRunSync()
  println(xI)
}

fun prompt(msg: String) = IO {
  print(msg)
}

fun readALine() = readLine() ?: ""

fun readAllLines(acc: List<String>) : List<String> {
  val line = readALine()
  return if (line.trim() == "")
    acc
  else
    readAllLines(listOf(line) + acc)
}

fun getFilename() : IO<Either<String, String>> = IO {
  val fname = readALine()
  if (fname == "")
    "Filename not provided".left()
  else
    fname.right()
}

fun readMultipleData() : IO<List<String>> = IO {
  readAllLines(listOf())
}

fun saveToFile(f: String, dataLines: List<String>) : IO<Option<String>> = IO {
  Try {
    File(f).bufferedWriter().use { of ->
      dataLines.forEach {
        of.write(it)
        of.newLine()
      }
    }
    None
  }.getOrElse { "Error Writing to file".some() }
}

tailrec fun readNumber() : IO<Option<Int>> = IO {
  Try {
    readLine()?.toInt()?.some() ?: None
  }.getOrElse { None }
}


fun main() {
//  val x = ioBinding {
//    prompt("Enter a number: ").bind()
//    val num = readNumber().bind()
//    num
//  }.attempt().unsafeRunSync().fold({ println ("Error: $it")}, { println("Number entered is $it")})

  ioBinding {
    prompt("Enter file name: ").bind()
    val fname = getFilename().bind()
    when (fname) {
      is Either.Left -> prompt(fname.a).bind()
      is Either.Right -> {
        val data = readMultipleData().bind()
        saveToFile(fname.b, data).bind()
      }
    }
  }.unsafeRunSync()
}