package org.njk.trials.fptrng.ioeither


import arrow.core.Either
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import arrow.data.EitherT
import arrow.data.fix
import arrow.effects.ForIO
import arrow.effects.IO
import arrow.effects.fix
import arrow.effects.instances.io.monad.monad
import arrow.instances.eithert.monad.monad
import arrow.syntax.function.pipe
import kotlinx.coroutines.Dispatchers
import java.math.BigDecimal

typealias Token = String

data class Fault(val message: String)
data class Gstr1(val gstin: String, val amount: BigDecimal)

fun <T> IO<Either<Fault,T>>.et(): EitherT<ForIO, Fault, T> = EitherT(this)
//fun <T> MonadContinuation<EitherTPartialOf<ForIO, Fault>, *>.etb(): (IO<Either<Fault,T>>) -> EitherT<ForIO, Fault, T> = { EitherT(it) }
//fun <T> MonadContinuation<EitherTPartialOf<ForIO, Fault>, *>.etb(block: () -> IO<Either<Fault,T>>) = { EitherT(block()).bind() }

fun <T> List<Either<Fault, T>>.compress(): Either<Fault, List<T>> =
    fold(listOf<T>().right()) { acc: Either<Fault, List<T>>, elem: Either<Fault, T> ->
      acc.flatMap { list ->
        elem.fold(
            { it.left() },
            { (list + it).right() }
        )
      }
    }


fun getGSTR1(gstin: String): IO<Either<Fault, Either<List<Gstr1>, Token>>> = IO {
  if (gstin.startsWith("T")) {
//    throw Exception("forced-exception")
    "TOKEN123".right().right()
//    Fault("forced-fault").left()
  } else
    listOf(Gstr1(gstin, BigDecimal("1000.00"))).left().right()
}

fun getFilesForToken(gstr1OrToken: Either<List<Gstr1>, Token>): IO<Either<Fault, Either<List<Gstr1>, List<String>>>> {
  return IO {
    gstr1OrToken.fold(
        { it.left().right() },
        { listOf("abc", "def").right().right() }
    )
  }
}

fun getGstr1ForFile(string: String): Either<Fault, Gstr1> {
  return Gstr1(string, BigDecimal("1000.00")).right()
}

fun getForTokenList(gstr1OrFileList: Either<List<Gstr1>, List<String>>): IO<Either<Fault, List<Gstr1>>> {
  return IO { gstr1OrFileList.fold(
      { it.right() },
      { it.map { getGstr1ForFile(it) }.compress() })
  }
}

fun runIO(gstin: String) {
  val result = EitherT.monad<ForIO, Fault>(IO.monad()).binding {
      val t = { getGSTR1(gstin).et()}.bindIn(Dispatchers.Default).bind()
      val gstr1sOrToken = getGSTR1(gstin).et().bind()
      val gstr1sOrFileList = getFilesForToken(gstr1sOrToken).et().bind()
      val gstr1s = getForTokenList(gstr1sOrFileList).et().bind()
      gstr1s
    }.fix()

  val result2 = EitherT.monad<ForIO, Fault>(IO.monad()).binding {
      (getGSTR1(gstin).et().bind()) pipe { getFilesForToken(it).et().bind() } pipe { getForTokenList(it).et().bind() }
    }.fix()

  try {
    println(result.value().fix().attempt().unsafeRunSync())
  } catch (e: Exception) {
    println("I caught an exception ${e}")
  }
}

fun run() {
  runIO("NoToken")
  runIO("Token")
}

fun main() {
  run()
}