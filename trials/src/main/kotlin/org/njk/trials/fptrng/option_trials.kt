package org.njk.trials.fptrng

import arrow.instances.ForEither
import arrow.core.*

import arrow.syntax.function.pipe
import arrow.instances.option.monad.binding as optionBinding
import arrow.instances.either.monad.binding as eitherBinding


/**
 * A -->(f: A->B) --> B -->(g: B->C) --> C -->(h:C->D) --> D
 * h o g o f = a.some().map(f).map(g).map(h)
 *
 * A -->(f: A->Option<B>) --> B -->(g: B->Option<C>) --> C -->(h:C->Option<D>) --> D
 * h o g o f = optAObject.flatMap(f).flatMap(g).flatMap(h)
 **/

val divideBy2: (Int) -> Option<Int> = { if (it % 2 == 0) (it / 2).some() else None }
val divideBy3: (Int) -> Option<Int> = { if (it % 3 == 0) (it / 3).some() else None }
val divideBy4: (Int) -> Option<Int> = { if (it % 4 == 0) (it / 4).some() else None }

fun divideBy2Either(n: Int): Either<String, Int> = if (n%2 == 0) (n/2).right() else "Not divisible by 2".left()
fun divideBy3Either(n: Int): Either<String, Int> = if (n%3 == 0) (n/3).right() else "Not divisible by 3".left()
fun divideBy4Either(n: Int): Either<String, Int> = if (n%4 == 0) (n/4).right() else "Not divisible by 4".left()

fun main() {
  println("Hello ${"World!".some()}")

  val n2: Int? = 3
  val someOfn2 = Option.fromNullable(n2)
  println(someOfn2.orNull())
  val noneInt = None as Option<Int>
  println(noneInt.map { it * 2 })
  println(Option.just(7).map { it * 2 })
  println(5.some().map { it * 2 })
  println(None.flatMap(divideBy2))
  println(5.some().flatMap(divideBy2))
  println(6.some().flatMap(divideBy2))

  println(noneInt.fold({ "None function" }, { (it * 2).toString() }))
  println(5.some().fold({ "None function" }, { (it * 2).toString() }))

  println("hello" pipe { "$it world" } pipe { it.toUpperCase() })

  println(24.some().flatMap(divideBy2).flatMap(divideBy3).flatMap(divideBy4))
  println(25.some().flatMap(divideBy2).flatMap(divideBy3).flatMap(divideBy4))

  //use map/flatmap when types line up
  //use monadic comprehension when types do not line up
  val start = 48
  val result = optionBinding {
      val a = divideBy2(start).bind()
      val b = divideBy3(a).bind()
      val c = divideBy4(a).bind()
      a + b + c
    }
  println(result)

  val result2 = ForEither<String>() extensions {
    binding {
      val a = divideBy2Either(48).bind()
      val b = divideBy3Either(a).bind()
      val c = divideBy4Either(b).bind()
      c
    }
  }
  println(result2)


  //Writing applicatives - Execute all functions in parallel and return all the values in one go
  val result3: Either<String, Tuple3<Int, Int, Int>> =
      ForEither<String>() extensions  {
        tupled(divideBy2Either(48), divideBy3Either(48), divideBy4Either(48)).fix()
      }
  println(result3)

  val result4: Either<String, Tuple3<Int, Int, Int>> =
      ForEither<String>() extensions  {
        tupled(divideBy2Either(48), divideBy3Either(50), divideBy4Either(48)).fix()
      }
  println(result4)

  // What is a functor? it has map such that
  // map(F[A], A -> B): F[B]
  // Option as functor
  // map(Option[A], A -> B): Option[B]
  // Either<L,R> as functor
  // map(Either<L,A>, A -> B): Either<L,B>
  // Either<L> is the functor .... Hence ForEither<String>() to instantiate a Either monad.
  // All monads are functors, but all functors are not monads
  // Either<R> as functor
  // map(Either<A, R>, A -> B): Either<B,R>

}
