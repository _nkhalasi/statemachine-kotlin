package org.njk.trials.fptrng

import arrow.instances.ForStateT
import arrow.instances.ForState
import arrow.core.*
import arrow.data.*
import arrow.instances.either.monad.monad
import arrow.instances.either.monadError.monadError
import arrow.instances.statet.monad.binding as stateTBinding

typealias DocNumber = String
typealias Originator = String
typealias Recipient = String

enum class DocState {
  CREATED, APPROVED, FUNDED
}
data class Document(val number: DocNumber, val originator: Originator, val recipient: Recipient, val docState: DocState)

sealed class DocUpdateError {
  class CannotApprove: DocUpdateError()
  class CannotFund: DocUpdateError()
}
typealias CannotApprove = DocUpdateError.CannotApprove
typealias CannotFund = DocUpdateError.CannotFund

fun approve() = State<Document, DocState> { doc ->
  doc.copy(
      docState = DocState.APPROVED
  ) toT DocState.APPROVED
}

fun approveE() = StateT<EitherPartialOf<DocUpdateError>, Document, DocState>(Either.monad()) { doc ->
  if (doc.docState == DocState.CREATED)
    (doc.copy(
        docState = DocState.APPROVED
    ) toT DocState.APPROVED).right()
  else
    Either.Left(CannotApprove())
}

fun funded() = State<Document, DocState> { doc ->
  if (doc.docState == DocState.APPROVED)
    doc.copy(
        docState = DocState.FUNDED
    ) toT DocState.FUNDED
  else
    doc toT doc.docState
}

fun fundedE() = StateT<EitherPartialOf<DocUpdateError>, Document, DocState>(Either.monad()) { doc ->
  if (doc.docState == DocState.APPROVED)
    (doc.copy(
        docState = DocState.FUNDED
    ) toT DocState.FUNDED).right()
  else
    Either.Left(CannotFund())
}

fun main(args: Array<String>) {
  val doc = Document("ABCD123", "SuplerA", "DealerA", DocState.CREATED)
  val seq1 = ForState<Document>() extensions {
    binding {
      val a = approve().bind()
      val b = funded().bind()
      b
    }.fix()
  }
  println(seq1.run(doc))

  val seq2 = ForState<Document>() extensions {
    binding {
      val b = funded().bind()
      b
    }.fix()
  }
  println(seq2.run(doc))

  val seq3 = ForStateT<EitherPartialOf<DocUpdateError>, Document, DocUpdateError>(Either.monadError<DocUpdateError>()) extensions {
    binding {
      val a = approveE().bind()
      val b = fundedE().bind()
      b
    }.fix()
  }
  println(seq3.runM(Either.monad<DocUpdateError>(), doc))

  val seq4 =
      ForStateT<EitherPartialOf<DocUpdateError>, Document, DocUpdateError>(Either.monadError<DocUpdateError>()) extensions {
        binding {
          val a = approveE().bind()
          val b = fundedE().bind()
          b
        }.fix()
  }
  println(seq4.runM(Either.monad<DocUpdateError>(), Document("DEFGH123", "SuplerA", "DealerA", DocState.FUNDED)))
}