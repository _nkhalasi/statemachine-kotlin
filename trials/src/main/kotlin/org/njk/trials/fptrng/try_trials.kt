package org.njk.trials.fptrng

import arrow.core.*
import arrow.instances.either.monad.flatMap
import arrow.instances.either.monad.binding

fun toNaturalInt(str: String) : Either<String, Int> =
    str.toInt().let { number ->
      if (number >= 0) number.right()
      else "Not a natural number".left()
    }


fun ensureEven(number: Int): Either<String, Int> =
    if (number % 2 == 0) number.right()
    else "Not an even number".left()

fun processNumber1(str: String) =
    toNaturalInt(str)
        .flatMap { ensureEven(it) }

fun main(args: Array<String>) {
  println(processNumber1("12"))
  println(processNumber1("11"))
  println(processNumber1("-1"))
  println("----------------------------")
  // catch exceptions. You can do this either at the outside most handler or inside each function depending on
  // how granular the error message needs to be
  println(Try { processNumber1("foo") }.getOrDefault { "Cannot convert to number".left() })
  // If the left hand side of the either is an exception you can convert it to an either quickly
  println(Try { processNumber1("foo") }.toEither())
  println("----------------------------")
  // you could also use it in a monad binding to capture exceptions at lower levels
  val result: Either<String, Int> = binding {
    val naturalNumber = Try { toNaturalInt("foo") }.getOrDefault{ "Cannot convert to number".left() }.bind()
    val evenNumber = ensureEven(naturalNumber).bind()
    evenNumber
  }
  println(result)
  // of course you could also use Try within each function block. Though I am not sure if thats the best strategy
}