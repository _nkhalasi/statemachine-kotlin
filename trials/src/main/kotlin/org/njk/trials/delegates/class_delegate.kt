package org.njk.trials.delegates

interface Person {
  fun printName()
}

class PersonImpl(val name: String) : Person {
  override fun printName() {
    println(name)
  }
}

class User(val person: Person):Person by person {
  override fun printName() {
    println("Printing name:")
    person.printName()
  }
}

fun main() {
  val person = PersonImpl("Mario Arias")
  person.printName()
  println()
  val user = User(person)
  user.printName()
}