package org.njk.trials.delegates

import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates

lateinit var notNullStr: String
lateinit var notInitStr: String
val myLazyVal: String by lazy {
  println("Just initialized")
  "My Lazy Val"
}

var myStr: String by Delegates.observable("<Initial Value>") { property, oldValue, newValue ->
  println("""Property `${property.name}` changed value from "$oldValue" to "$newValue"""")
}

var myIntEven: Int by Delegates.vetoable(0) { property, oldValue, newValue ->
  println("${property.name}: $oldValue -> $newValue")
  newValue % 2 == 0
}

data class Book(val delegate: Map<String, Any?>) {
  val name: String  by delegate
  val authors: String  by delegate
  val pageCount: Int  by delegate
  val publicationDate: Date  by delegate
  val publisher: String  by delegate
}

fun main() {
  notNullStr = "Initial Value"
  println(notNullStr)
//  println(notInitStr)
  println(myLazyVal)
  myStr = "Change Value"
  myStr = "Change Value again"

  myIntEven = 6
  myIntEven = 3
  println("myIntEven = $myIntEven")

  val map1 = mapOf(
      Pair("name", "Reactive	Programming	in	Kotlin"),
      Pair("authors", "Rivu	Chakraborty"),
      Pair("pageCount", 400),
      Pair("publicationDate", SimpleDateFormat("yyyy/mm/dd").parse("2017/12/05")),
      Pair("publisher", "Packt")
  )
  val map2 = mapOf(
      "name" to "Kotlin	Blueprints",
      "authors" to "Ashish	Belagali,	Hardik	Trivedi,	Akshay	Chordiya",
      "pageCount" to 250,
      "publicationDate" to SimpleDateFormat("yyyy/mm/dd").parse("2017/12/05"),
      "publisher" to "Packt"
  )

  val book1 = Book(map1)
  val book2 = Book(map2)

  println("Book1	$book1	nBook2	$book2")
}