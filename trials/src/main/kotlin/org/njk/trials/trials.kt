package org.njk.trials

import java.io.File
import java.time.Instant
import java.util.*


data class Person(val fname: String, val lname: String) {
//    var mname: String? = null
//
//    init {
//        initializeMname()
//    }
//
//    fun initializeMname() {
//        mname = "Whatever"
//    }
}

object PersonCache {
  val persons = mutableListOf<Person>()
  fun initialize() {
    persons += listOf(Person("Naresh", "Khalasi"), Person("Krishang", "Khalasi"))
  }
}

fun printPersons() {
  println("${PersonCache.persons}")
}

fun main(args: Array<String>) {
//  printPersons()
//  PersonCache.initialize()
//  printPersons()
//  val _now = Instant.now()
//  println(_now)
//  println(_now.plusMillis(3600*1000))
//  println(Date.from(_now))
//  println(_now)

  File("trials/_data/abc").listFiles().map { it.name }.sorted().forEach { println(it) }

  val files = File("trials/_data/abc").listFiles().map { "\"${it.name}\"" }.joinToString(",")
  val sql = """
      select errors, upload_filename,processed_filename,processing_status,created_on, last_updated
      from inbound_files
      where upload_filename in ($files) \G
    """.trimIndent()
  println(sql)
}