package org.njk.trials

import org.apache.http.*
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicNameValuePair
import org.apache.http.protocol.HTTP
import org.apache.http.util.EntityUtils
import java.io.File
import java.security.KeyStore

fun main(args: Array<String>) {
  val vfs_host = "gamma-demo.vayananet.com"
  val vfs_port = 443
  val vfs_url_path = "vauploads"
  val vfs_user_id = "BankA/banka1"
  val vfs_password = "Password@12"
  val targetHost = HttpHost(vfs_host, vfs_port, "http")
  val nvps = listOf(BasicNameValuePair("file", "<head></head>"))
  val trustStore = KeyStore.getInstance(KeyStore.getDefaultType()).apply {
    load(File("gamma-demo.vayananet.com.jks").inputStream(), "nopassword".toCharArray())
  }
  val socketFactory = SSLSocketFactory(trustStore)
  val sch = Scheme("https", socketFactory, targetHost.getPort());
  val httpclient = DefaultHttpClient().apply {
    credentialsProvider.setCredentials(
        AuthScope(targetHost.hostName, targetHost.port),
        UsernamePasswordCredentials(vfs_user_id, vfs_password)
    )
    connectionManager.schemeRegistry.register(sch)
  }
  val url = "https://$vfs_host:$vfs_port/$vfs_url_path"
  val httpost = HttpPost(url).apply {
    setHeader("accept", "application/xml")
    entity = UrlEncodedFormEntity(nvps, HTTP.UTF_8)
  }
  val response = httpclient.execute(httpost)
  val sl = response.statusLine
  println(sl.statusCode)
  if (sl.statusCode != 200) {
    println(EntityUtils.toString(response.entity, "UTF-8"))
    response.entity.consumeContent()
  }
}

