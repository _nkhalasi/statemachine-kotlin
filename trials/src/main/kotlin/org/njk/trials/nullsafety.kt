package org.njk.trials


fun hello(name: String) {
    println("Hello $name")
}

fun main(args: Array<String>) {
    val str = "Kotlin"
    hello(str)

    val maybeStr: String? = "Maybe?"
//    hello(maybeStr) //doesn't COMPILE
    if (maybeStr != null)
        hello(maybeStr)

    val maybeStr1: String? = "Maybe1?"
    hello(maybeStr1 as String)

    val maybeStr2: String? = "Maybe2?"
    hello(maybeStr2!!)

    val maybeStr3: String? = null
    hello(maybeStr3!!)
}