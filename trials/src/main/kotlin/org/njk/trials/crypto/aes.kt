package org.njk.trials.crypto

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.InputStream
import java.nio.file.Paths
import java.security.MessageDigest
import java.security.SecureRandom
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

val b64Encoder = Base64.getEncoder()
val b64Decoder = Base64.getDecoder()

typealias B64EncodedString = String

fun salt(): B64EncodedString =
    b64Encoder.encodeToString(
        ByteArray(16).apply {
          SecureRandom().nextBytes(this)
        }
    )

// Ideally the IV should be generated during each encryption and shared with the counterparty to use during decryption.
// One way is to store the IV at the beginning of the encrypted output.
fun iv(size: Int): B64EncodedString =
    b64Encoder.encodeToString(
        ByteArray(size).apply {
          SecureRandom().nextBytes(this)
        }
    )

//fun iv(secretCipher: Cipher): B64EncodedString =
//    Base64.getEncoder().encodeToString(
//        secretCipher.let {
//          val params = it.parameters
//          params.getParameterSpec(IvParameterSpec::class.java).iv
//        }
//    )

fun pbkdf2KeySpec(pwd: String, encodedSalt: B64EncodedString) =
    PBEKeySpec(pwd.toCharArray(), b64Decoder.decode(encodedSalt), 100000, 256).let {
      val hash = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256").generateSecret(it).encoded
      SecretKeySpec(hash, "AES")
    }

fun md5Secret(pwd: String): SecretKeySpec =
    MessageDigest.getInstance("SHA-256").let {
      val key = it.digest(pwd.toByteArray())
      SecretKeySpec(Arrays.copyOf(key, 16), "AES")
    }

//tailrec fun BufferedInputStream.encOrDec(cipher: Cipher, buffer: ByteArray): ByteArray {
//  val len = read(buffer)
//  return if (len > 0) {
//    val buf = cipher.update(buffer, 0, len)
//    encOrDec(cipher, buf + ByteArray(1024))
//  } else buffer
//}

tailrec fun BufferedInputStream.encrypt(cipher: Cipher, fos: BufferedOutputStream) {
  val buffer = ByteArray(1024)
  val len = read(buffer)
  if (len > 0) {
    fos.write(
        cipher.update(buffer, 0, len)
    )
    encrypt(cipher, fos)
  }
}

tailrec fun BufferedInputStream.decrypt(cipher: Cipher, fos: BufferedOutputStream) {
  val buffer = ByteArray(1024)
  val len = read(buffer)
  if (len > 0) {
    fos.write(
        cipher.update(buffer, 0, len)
    )
    decrypt(cipher, fos)
  }
}

fun loadSecrets(fname: String): Pair<String, B64EncodedString> =
    Paths.get(fname).toFile().bufferedReader().use { f ->
      val p = f.readLine().let {
        it.slice(IntRange(2, it.length-1))
      }
      val s = f.readLine().let {
        it.slice(IntRange(2, it.length-1))
      }
      Pair(p, s)
    }

sealed class CipherCreatorInputs
data class AesCbcCipherCreatorInputs(val pwd: String, val encodedSalt: B64EncodedString, val encodedIv: B64EncodedString="") : CipherCreatorInputs() {
  fun withIv(encodedIv: B64EncodedString) = AesCbcCipherCreatorInputs(pwd, encodedSalt, encodedIv)
}
data class AesEcbCipherCreatorInputs(val pwd: String) : CipherCreatorInputs()
data class AesGcmCipherCreatorInputs(val pwd: String, val encodedIv: B64EncodedString=""): CipherCreatorInputs() {
  fun withIv(encodedIv: B64EncodedString) = AesGcmCipherCreatorInputs(pwd, encodedIv)
}
data class AesCtrCipherCreatorInputs(val pwd: String, val encodedSalt: B64EncodedString, val encodedIv: B64EncodedString=""): CipherCreatorInputs() {
  fun withIv(encodedIv: B64EncodedString) = AesCtrCipherCreatorInputs(pwd, encodedSalt, encodedIv)
}

typealias CipherCreator<T> = (T) -> Cipher
val aesCbcEncryptCipher: CipherCreator<AesCbcCipherCreatorInputs> = { inp ->
  Cipher.getInstance("AES/CBC/PKCS5Padding").apply {
    init(Cipher.ENCRYPT_MODE, pbkdf2KeySpec(inp.pwd, inp.encodedSalt), IvParameterSpec(b64Decoder.decode(iv(16))))
  }
}
val aesCbcDecryptCipher: CipherCreator<AesCbcCipherCreatorInputs> = { inp ->
  Cipher.getInstance("AES/CBC/PKCS5Padding").apply {
    init(Cipher.DECRYPT_MODE, pbkdf2KeySpec(inp.pwd, inp.encodedSalt), IvParameterSpec(b64Decoder.decode(inp.encodedIv)))
  }
}
val aesEcbEncryptCipher: CipherCreator<AesEcbCipherCreatorInputs> = { inp ->
  md5Secret(inp.pwd).let {
    Cipher.getInstance("AES/ECB/PKCS5Padding").apply {
      init(Cipher.ENCRYPT_MODE, it)
    }
  }
}
val aesEcbDecryptCipher: CipherCreator<AesEcbCipherCreatorInputs> = { inp ->
  md5Secret(inp.pwd).let {
    Cipher.getInstance("AES/ECB/PKCS5Padding").apply {
      init(Cipher.DECRYPT_MODE, it)
    }
  }
}
val aesGcmEncryptCipher: CipherCreator<AesGcmCipherCreatorInputs> = { inp ->
  md5Secret(inp.pwd).let {
    Cipher.getInstance("AES/GCM/NoPadding").apply {
      init(Cipher.ENCRYPT_MODE, it, GCMParameterSpec(12 * 8, b64Decoder.decode(iv(12))))
    }
  }
}
val aesGcmDecryptCipher: CipherCreator<AesGcmCipherCreatorInputs> =  { inp ->
  md5Secret(inp.pwd).let {
    Cipher.getInstance("AES/GCM/NoPadding").apply {
      init(Cipher.DECRYPT_MODE, it, GCMParameterSpec(12 * 8, b64Decoder.decode(inp.encodedIv)))
    }
  }
}
val aesCtrEncryptCipher: CipherCreator<AesCtrCipherCreatorInputs> = { inp ->
  Cipher.getInstance("AES/CTR/NoPadding").apply {
    init(Cipher.ENCRYPT_MODE, pbkdf2KeySpec(inp.pwd, inp.encodedSalt), IvParameterSpec(b64Decoder.decode(iv(16))))
  }
}
val aesCtrDecryptCipher: CipherCreator<AesCtrCipherCreatorInputs> =  { inp ->
  Cipher.getInstance("AES/CTR/NoPadding").apply {
    init(Cipher.DECRYPT_MODE, pbkdf2KeySpec(inp.pwd, inp.encodedSalt), IvParameterSpec(b64Decoder.decode(inp.encodedIv)))
  }
}


fun <T: CipherCreatorInputs> encryptFile(fname: String, creds: T, fn: CipherCreator<T>): Unit =
    fn(creds).let { cipher ->
      Paths.get(fname).toFile().inputStream().buffered().use { inFile ->
        Paths.get("$fname.enc").toFile().outputStream().buffered().use { outFile ->
          cipher.iv?.let { outFile.write(it) } //applicable only incase of CBC & GCM mode only
          inFile.encrypt(cipher, outFile)
          outFile.write(cipher.doFinal())
        }
      }
    }

private fun <T: CipherCreatorInputs> newCreds(creds: T, inf: InputStream): T {
  return when (creds) {
    is AesCbcCipherCreatorInputs -> {
      ByteArray(16)
          .apply { inf.read(this) }
          .let { creds.withIv(b64Encoder.encodeToString(it)) as T }
    }
    is AesGcmCipherCreatorInputs -> {
      ByteArray(12)
          .apply { inf.read(this) }
          .let { creds.withIv(b64Encoder.encodeToString(it)) as T }
    }
    is AesCtrCipherCreatorInputs -> {
      ByteArray(16)
          .apply { inf.read(this) }
          .let { creds.withIv(b64Encoder.encodeToString(it)) as T }
    }
    else -> creds
  }
}

fun <T: CipherCreatorInputs> decryptFile(fname: String, creds: T, fn: CipherCreator<T>): Unit =
    Paths.get("$fname.enc").toFile().inputStream().buffered().use { inf ->
      Paths.get("$fname.dec").toFile().outputStream().buffered().use { outf ->
        val creds1 = newCreds(creds, inf)
        val cipher = fn(creds1)
        inf.decrypt(cipher, outf)
        outf.write(cipher.doFinal())
      }
    }

fun main() {
  val p = "secret"
  val s = salt()
  Paths.get("keysecrets.txt").toFile().bufferedWriter().use {
    it.write("p:$p")
    it.newLine()
    it.write("s:$s")
    it.newLine()
  }

//  val (p, s) = loadSecrets("keysecrets.txt")

  encryptFile("file1.txt", AesCbcCipherCreatorInputs(p, s), aesCbcEncryptCipher)
  decryptFile("file1.txt", AesCbcCipherCreatorInputs(p, s), aesCbcDecryptCipher)
//  decryptFile("sample.xlsx",, AesCbcCipherCreatorInputs(p, s), aesCbcDecryptCipher)

  encryptFile("file2.txt", AesEcbCipherCreatorInputs(p), aesEcbEncryptCipher)
  decryptFile("file2.txt", AesEcbCipherCreatorInputs(p), aesEcbDecryptCipher)

  encryptFile("file3.txt", AesGcmCipherCreatorInputs(p), aesGcmEncryptCipher)
  decryptFile("file3.txt", AesGcmCipherCreatorInputs(p), aesGcmDecryptCipher)

  encryptFile("file4.txt", AesCtrCipherCreatorInputs(p, s), aesCtrEncryptCipher)
  decryptFile("file4.txt", AesCtrCipherCreatorInputs(p, s), aesCtrDecryptCipher)
}
