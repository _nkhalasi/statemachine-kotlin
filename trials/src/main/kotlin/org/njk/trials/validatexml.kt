package org.njk.trials

import org.xml.sax.InputSource
import org.xml.sax.helpers.DefaultHandler
import org.xml.sax.helpers.XMLReaderFactory


const val emptyPayload = """<?xml version="1.0" encoding="UTF-8"?>
<FundingRequestStatus>
    <header>
        <source>
            vayana
        </source>
        <description>
            Funding Request Status Updates
        </description>
        <datetime>
            2015-10-13T11:14:19
        </datetime>
    </header>
    <FundingRequests/>
</FundingRequestStatus>
"""



fun isValidXML(xmlString: String): Boolean =
    try {
        XMLReaderFactory.createXMLReader().let { reader ->
            reader.contentHandler = DefaultHandler()
            reader.parse(InputSource(xmlString.byteInputStream()))
            true
        }
    } catch (e: Exception) {
        false
    }

fun main(args: Array<String>) {
    println(isValidXML(emptyPayload))
    println(isValidXML("123"))
}