package org.njk.trials

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import org.apache.pdfbox.text.PDFTextStripperByArea

import java.io.File
import java.io.IOException

object ReadPdf {

    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {

        PDDocument.load(File("/home/nkhalasi/myprojects/statemachine-kotlin/trials/src/main/resources/lineng-invoice-20180104-02B.pdf")).use { document ->
            document.javaClass
            if (!document.isEncrypted) {
                val stripper = PDFTextStripperByArea().apply {
                    sortByPosition = true
                }
                println("----------------")
                stripper.regions.forEach {
                    println(it)
                }
                println("----------------")

                val tStripper = PDFTextStripper()
                val pdfFileInText = tStripper.getText(document)
//                println(pdfFileInText)
//                println("----------------")
                // split by whitespace
                val lines = pdfFileInText.split("\\r?\\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                for (line in lines) {
                    println(line)
                }

            }

        }

    }
}