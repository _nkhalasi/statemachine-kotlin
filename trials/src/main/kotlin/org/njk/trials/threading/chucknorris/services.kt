package org.njk.trials.threading.chucknorris.services

import kotlinx.coroutines.*
import org.njk.trials.threading.chucknorris.clients.*
import org.njk.trials.threading.chucknorris.models.Fact
import org.njk.trials.threading.chucknorris.models.User
import org.njk.trials.threading.chucknorris.models.UserId
import java.time.Duration
import java.time.LocalDateTime

interface UserService {
  fun getFact(id: UserId): Fact
}

class SynchronousUserService(private val userClient: UserClient,
                             private val factClient: FactClient,
                             private val userRepository: UserRepository,
                             private val factRepository: FactRepository) : UserService {
  override fun getFact(id: UserId): Fact {
    val user = userRepository.getUserById(id)
    return if (user == null) {
      val userFromService = userClient.getUser(id)
      userRepository.insertUser(userFromService)
      getFact(userFromService)
    } else {
      factRepository.getFactByUserId(id) ?: getFact(user)
    }
  }

  private fun getFact(user: User): Fact {
    val fact = factClient.getFact(user)
    factRepository.insertFact(fact)
    return fact
  }
}

//class CoroutineUserService(private val userClient: UserClient,
//                           private val factClient: FactClient,
//                           private val userRepository: UserRepository,
//                           private val factRepository: FactRepository) : UserService {
//  override fun getFact(id: UserId): Fact = runBlocking {
//    val user = async { userRepository.getUserById(id) }.await()
//    if (user == null) {
//      val userFromService = async { userClient.getUser(id) }.await()
//      launch { userRepository.insertUser(userFromService) }
//      getFact(userFromService)
//    } else {
//      async { factRepository.getFactByUserId(id) ?: getFact(user) }.await()
//    }
//  }
//
//  private suspend fun getFact(user: User): Fact {
//    val fact = GlobalScope.async { factClient.getFact(user) }
//    runBlocking {
//      launch { factRepository.insertFact(fact.await()) }
//    }
//    return fact.await()
//  }
//}

fun main(args: Array<String>) {

  fun inTime(fn: () -> Fact): Pair<Fact, Duration> {
    val startT = LocalDateTime.now()
    return Pair(fn(), Duration.between(startT, LocalDateTime.now()))
  }

  fun execute(userService: UserService, id: Int) {
    val (fact, time) = inTime {
      userService.getFact(id)
    }
    println("fact = $fact")
    println("time = $time ms.")
  }

  val userClient = MockUserClient()
  val factClient = MockFactClient()
  val userRepository = MockUserRepository()
  val factRepository = MockFactRepository()
  val userService = SynchronousUserService(userClient, factClient, userRepository, factRepository)
  execute(userService, 1)
  execute(userService, 2)
  execute(userService, 1)
  execute(userService, 2)
  execute(userService, 3)
  execute(userService, 4)
  execute(userService, 5)
  execute(userService, 10)
  execute(userService, 100)
}