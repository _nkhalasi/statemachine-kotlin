package org.njk.trials.threading

import kotlinx.coroutines.*
import java.io.File

fun main(args: Array<String>) = runBlocking<Unit> {
  val jobs = arrayListOf<Job>()
  jobs += launch(Dispatchers.Unconfined) { // not confined -- will work with main thread
    println("      'Unconfined': I'm working in thread ${Thread.currentThread().name}")
  }
  jobs += launch(coroutineContext) { // context of the parent, runBlocking coroutine
    println("'coroutineContext': I'm working in thread ${Thread.currentThread().name}")
  }
  jobs += launch(Dispatchers.Default) { // will get dispatched to ForkJoinPool.commonPool (or equivalent)
    println("      'CommonPool': I'm working in thread ${Thread.currentThread().name}")
  }
  jobs += launch(newSingleThreadContext("MyOwnThread")) { // will get its own new thread
    println("          'newSTC': I'm working in thread ${Thread.currentThread().name}")
  }
  jobs.forEach { it.join() }

  println(
      File("/home/nkhalasi/vayana/projects/pluto-filesplitter/src/test/resources/multi_entity_sample.csv").nameWithoutExtension
  )
  println(
      File("/home/nkhalasi/vayana/projects/pluto-filesplitter/src/test/resources/multi_entity_sample.csv").name
  )
}
