package org.njk.trials.threading.chucknorris.models

enum class Gender {
  MALE, FEMALE;

  companion object {
    fun valueOfIgnoreCase(name: String): Gender = valueOf(name.toUpperCase())
  }
}
typealias  UserId = Int

data class User(val id: UserId, val firstName: String, val lastName: String, val gender: Gender)
data class Fact(val id: Int, val value: String, val user: User? = null)
