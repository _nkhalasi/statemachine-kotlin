package org.njk.trials.threading

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) = runBlocking {
  val jobs = List(1000000) {
    launch {
      delay(1000)
      print('.')
    }
  }
  jobs.forEach { job -> job.join() }
}
