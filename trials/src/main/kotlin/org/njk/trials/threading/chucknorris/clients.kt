package org.njk.trials.threading.chucknorris.clients

import  com.github.salomonbrys.kotson.*
import  com.google.gson.GsonBuilder
import  org.http4k.client.ApacheClient
import org.http4k.core.Method
import org.http4k.core.Request
import org.njk.trials.threading.chucknorris.models.Fact
import org.njk.trials.threading.chucknorris.models.Gender
import org.njk.trials.threading.chucknorris.models.User
import org.njk.trials.threading.chucknorris.models.UserId
import java.util.*

interface UserClient {
  fun getUser(id: UserId): User
}

interface FactClient {
  fun getFact(user: User): Fact
}

abstract class WebClient {
  protected val apacheClient = ApacheClient()
  protected val gson = GsonBuilder()
      .registerTypeAdapter<User> {
        deserialize { des ->
          val json = des.json
          User(json["info"]["seed"].int,
              json["results"][0]["name"]["first"].string.capitalize(),
              json["results"][0]["name"]["last"].string.capitalize(),
              Gender.valueOfIgnoreCase(json["results"][0]["gender"].string))
        }
      }
      .registerTypeAdapter<Fact> {
        deserialize { des ->
          val json = des.json
          Fact(json["value"]["id"].int,
              json["value"]["joke"].string)
        }
      }.create()!!
}

class Http4KUserClient : WebClient(), UserClient {
  override fun getUser(id: UserId): User {
    return gson.fromJson(apacheClient(Request(Method.GET, "https://randomuser.me/api")
        .query("seed", id.toString()))
        .bodyString())
  }
}

class Http4KFactClient : WebClient(), FactClient {
  override fun getFact(user: User): Fact {
    return gson.fromJson<Fact>(apacheClient(Request(Method.GET, "http://api.icndb.com/jokes/random")
        .query("firstName", user.firstName)
        .query("lastName", user.lastName))
        .bodyString())
        .copy(user = user)
  }
}

interface UserRepository {
  fun getUserById(id: UserId): User?
  fun insertUser(user: User)
}

interface FactRepository {
  fun getFactByUserId(id: UserId): Fact?
  fun insertFact(fact: Fact)
}

class MockUserRepository : UserRepository {
  private val users = hashMapOf<UserId, User>()
  override fun getUserById(id: UserId): User? {
    println("MockUserRepository.getUserById")
    Thread.sleep(200)
    return users[id]
  }

  override fun insertUser(user: User) {
    println("MockUserRepository.insertUser")
    Thread.sleep(200)
    users[user.id] = user
  }
}

class MockFactRepository : FactRepository {
  private val facts = hashMapOf<UserId, Fact>()
  override fun getFactByUserId(id: UserId): Fact? {
    println("MockFactRepository.getFactByUserId")
    Thread.sleep(200)
    return facts[id]
  }

  override fun insertFact(fact: Fact) {
    println("MockFactRepository.insertFact")
    Thread.sleep(200)
    facts[fact.user?.id ?: 0] = fact
  }
}

class MockUserClient : UserClient {
  override fun getUser(id: UserId): User {
    println("MockUserClient.getUser")
    Thread.sleep(500)
    return User(id, "Foo", "Bar", Gender.FEMALE)
  }
}

class MockFactClient : FactClient {
  override fun getFact(user: User): Fact {
    println("MockFactClient.getFact")
    Thread.sleep(500)
    return Fact(Random().nextInt(), "FACT	${user.firstName},	${user.lastName}", user)
  }
}