package org.njk.trials.oltu

import org.apache.oltu.oauth2.client.OAuthClient
import org.apache.oltu.oauth2.client.URLConnectionClient
import org.apache.oltu.oauth2.client.request.OAuthClientRequest
import org.apache.oltu.oauth2.client.response.GitHubTokenResponse
import org.apache.oltu.oauth2.common.OAuthProviderType
import org.apache.oltu.oauth2.common.message.types.GrantType
import java.util.*


fun main(args: Array<String>) {
    val NETWORK_NAME = "GitHub"

    val redirectUrl = "http://localhost:8000/reljio/auth"
    val clientId = "9ce6203a80843b1be8d1"
    val clientSecret = "e3545e8cd85e2dd936abcd7677586de59cd4345d"
    val secretState = "secret" + Random().nextInt(999999)
    val authRequest = OAuthClientRequest
                    .authorizationProvider(OAuthProviderType.GITHUB)
                    .setClientId(clientId)
                    .setState(secretState)
                    .setRedirectURI(redirectUrl)
                    .buildQueryMessage()
    val _in = Scanner(System.`in`, "UTF-8")

    println("=== $NETWORK_NAME's OAuth Workflow ===")
    println()

    println("Fetching the Authorization URL...")
    val authUrl = authRequest.locationUri
    println("Go to the Authorization URL!")
    println("Now go and authorize ScribeJava here:")
    println(authUrl)
    println("And paste the authorization code here")
    println(">>")
    val code = _in.nextLine()
    println()

    println("And paste the state from server here. We have set 'secretState'='" + secretState + "'.")
    println(">>")
    val value = _in.nextLine()

    if (secretState.equals(value))
        println("State value does match!")
    else {
        println("Ooops, state value does not match!")
        println("Expected = $secretState, Got = $value")
        println()
    }

    println("Trading the Request Token for an Access Token...")
    val request = OAuthClientRequest
                    .tokenProvider(OAuthProviderType.GITHUB)
                    .setGrantType(GrantType.REFRESH_TOKEN)
                    .setClientId(clientId)
                    .setClientSecret(clientSecret)
                    .setRedirectURI(redirectUrl)
                    .setCode(code)
                    .buildQueryMessage()

    val client = OAuthClient(URLConnectionClient())
    val oauthResponse = client.accessToken(request, GitHubTokenResponse::class.java)
    println("Got the Access Token!")
    println("(accessToken=${oauthResponse.accessToken}, expiresIn=${oauthResponse.expiresIn}, refreshToken=${oauthResponse.refreshToken}, body=${oauthResponse.body})")
    println()
}


/*
Fetching the Authorization URL...
Go to the Authorization URL!
Now go and authorize ScribeJava here:
https://github.com/login/oauth/authorize?state=secret478359&redirect_uri=http%3A%2F%2Flocalhost%3A8000&client_id=9ce6203a80843b1be8d1
And paste the authorization code here
>>
6f66049abb17c3dcbbf6

And paste the state from server here. We have set 'secretState'='secret478359'.
>>
secret478359
State value does match!
Trading the Request Token for an Access Token...
Got the Access Token!
(accessToken=7087ddd4fa26342d81495b64a2d1e0b75eaf5217, expiresIn=null, refreshToken=null)


Process finished with exit code 0

 */


/*

Fetching the Authorization URL...
Go to the Authorization URL!
Now go and authorize ScribeJava here:
https://github.com/login/oauth/authorize?state=secret25287&redirect_uri=http%3A%2F%2Flocalhost%3A8000&client_id=9ce6203a80843b1be8d1
And paste the authorization code here
>>
5cd944306355371f8b16

And paste the state from server here. We have set 'secretState'='secret25287'.
>>
secret25287
State value does match!
Trading the Request Token for an Access Token...
Got the Access Token!
(accessToken=7087ddd4fa26342d81495b64a2d1e0b75eaf5217, expiresIn=null, refreshToken=null)


Process finished with exit code 0
 */

/*
Fetching the Authorization URL...
Go to the Authorization URL!
Now go and authorize ScribeJava here:
https://github.com/login/oauth/authorize?state=secret978301&redirect_uri=http%3A%2F%2Flocalhost%3A8000&client_id=9ce6203a80843b1be8d1
And paste the authorization code here
>>
9cbfbedccf3b6d953356

And paste the state from server here. We have set 'secretState'='secret978301'.
>>
secret978301
State value does match!
Trading the Request Token for an Access Token...
Got the Access Token!
(accessToken=7087ddd4fa26342d81495b64a2d1e0b75eaf5217, expiresIn=null, refreshToken=null, body=access_token=7087ddd4fa26342d81495b64a2d1e0b75eaf5217&scope=&token_type=bearer)


Process finished with exit code 0

 */