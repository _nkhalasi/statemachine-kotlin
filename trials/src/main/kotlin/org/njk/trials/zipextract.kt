package org.njk.trials.zip

import java.io.File
import java.io.FileOutputStream
import java.nio.file.Path
import java.util.zip.ZipInputStream

/**
 * Created by nkhalasi on 4/3/17.
 */


tailrec fun ZipInputStream.extract(outputDir: Path, extractedFiles: List<Path>) : List<Path> =
    nextEntry?.let { ze ->
        val zf = outputDir.resolve(ze.name).apply {
            toFile().outputStream().use {
                fileForZipEntry(it)
            }
        }
        extract(outputDir, extractedFiles+listOf(zf))
    } ?: extractedFiles

tailrec fun ZipInputStream.fileForZipEntry(fos: FileOutputStream) {
    val buffer = ByteArray(1024)
    val len = read(buffer)
    if (len > 0) {
        fos.write(buffer, 0, len)
        fileForZipEntry(fos)
    }
}

fun ZipInputStream.list() : List<String> =
    this@list.listEntries(listOf())

fun ZipInputStream.listEntries(acc: List<String>) : List<String> =
    nextEntry?.let { ze ->
      listEntries(acc + listOf(ze.name))
    } ?: acc
