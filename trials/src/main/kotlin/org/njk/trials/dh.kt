package org.njk.trials

import java.math.BigInteger
import java.security.*
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyAgreement
import javax.crypto.spec.DHParameterSpec
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

fun generateDHParams(): DHParameterSpec {
    val algorithmParameterGenerator = AlgorithmParameterGenerator.getInstance("DiffieHellman")
    algorithmParameterGenerator.init(2048)
    val algorithmParameters = algorithmParameterGenerator.generateParameters()
    val dhParams = algorithmParameters.getParameterSpec(DHParameterSpec::class.java) as DHParameterSpec
    return dhParams
}

fun genKeys(params: DHParameterSpec): KeyPair =
    KeyPairGenerator.getInstance("DH").let { it.initialize(params, SecureRandom()); it.generateKeyPair() }

fun generateSecret(private: PrivateKey, public: PublicKey): ByteArray =
    Arrays.copyOf(MessageDigest.getInstance("SHA-256").digest(with(KeyAgreement.getInstance("DH")) {
        init(private); doPhase(public, true); generateSecret()}), 16)


fun encrypt(text: String, secret: ByteArray, iv: String): ByteArray =
    Cipher.getInstance("AES/CBC/PKCS5Padding").let {
        it.init(Cipher.ENCRYPT_MODE, SecretKeySpec(secret, "AES"), IvParameterSpec(iv.toByteArray()))
        it.doFinal(text.toByteArray())
    }


fun decrypt(encrypted: ByteArray, secret: ByteArray, iv: String): String =
    String(Cipher.getInstance("AES/CBC/PKCS5Padding").let {
        it.init(Cipher.DECRYPT_MODE, SecretKeySpec(secret, "AES"), IvParameterSpec(iv.toByteArray()))
        it.doFinal(encrypted) })


fun main(args: Array<String>) {

    val IV = "ikluhweurhwerlkj";

    // Common code at both ends
    val p = BigInteger("19139922756800617836495976887392428641687750312687402458551884205146430125705413174899005979749667162907064470837407516756139880531710731518872301617456498238014038062707622422019193000766342681616108477201628439189820038660297901592517938098239795810346034827302338451720932425794196109860857090909425347864719722592403854402211130358626102486735969354366576297321135311909413539098474078049460942762553030563413617324235358772300888197625135155356233735270883964214665900773549835261476325682342899604850757602985168382755335043265007335473242162076440395103956401150670201674982486253251063051645743021076312265571")
    val g = BigInteger("18142478541796541615014299913383668960227240075628937278985755648761479726100345363063806349846590813341679649214813687531442253789907664382133236541098961759502160456175269432587865147271265668282389881075523155643021593040929071090394067233835363258657988173636135276381507000451774342762949255701129717198469353568102281055454878678279236010778981661263547793251234692662438922022348073646662841093321722040859991234141324968021118061101464353732059296354126034694867804529156545855356063465824822114471315444316167681123857373053077723167616749904031313427381554730723582193339231957679119065343657950590302068872")
    val l = 2047
    val params = DHParameterSpec(p,g,l)

    // Bank end. Could be one time, per file transfer
    val bankpair = genKeys(params)
    val bankpublic = bankpair.public  // this can be shared with vayana

    // Vayana end. For each file.
    val vayanapair = genKeys(params)
    val vayanapublic = vayanapair.public // this can be shared with bank

    // Text to encrypt
    val text = "the quick brown fox jumped over the lazy dog"

    // Generate secret using vayana private key and bank public key
    val secret = generateSecret(vayanapair.private, bankpair.public)

    // encrypt data
    val encrypted = encrypt(text,secret,IV)

    // the above is sent to bank. Bank regenerates shared secret as follows

    val secret2 = generateSecret(bankpair.private, vayanapair.public)

    val decrypted = decrypt(encrypted,secret2, IV)

    println(decrypted)
}