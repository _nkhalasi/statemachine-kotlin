package org.njk.trials

import java.net.URL
import java.security.KeyStore
import java.security.SecureRandom
import javax.net.ssl.*

/**
 * Use the following VM arguments
 * -Djavax.net.debug=ssl:handshake
 * -Djavax.net.ssl.keyStoreType=jks
 * -Djavax.net.ssl.keyStore=/home/nkhalasi/vayana/projects/mater-kotlin/src/main/resources/prod_keystore/ssl-cert.jks
 * -Djavax.net.ssl.keyStorePassword=xxxx
 *
 * Steps for generating a client certificate
 * 1. openssl genrsa -out vayanainteg.key 2048
 * 2. openssl req -key vayanainteg.key -new -out vayanainteg.req
 * 3. vi vayanainteg.cnf
 * 4. openssl x509 -req -days 1830 -in vayanainteg.req -signkey vayanainteg.key -out vayanainteg.crt -extfile vayanainteg.cnf -extensions ssl_client
 * 5. openssl x509 -text -noout -in vayanainteg.crt
 * 6. openssl pkcs12 -export -in vayanainteg.crt -inkey vayanainteg.key -name "vayananet.com" -out vayanainteg.p12
 * 7. keytool -importkeystore -deststorepass changeit -destkeystore vayanainteg.jks -srckeystore vayanainteg.p12 -srcstoretype PKCS12
 * 8. keytool -v -list -keystore vayanainteg.jks
 * 
 * Contents of vayanainteg.cnf
 * [ ssl_client ]
 * basicConstraints = CA:FALSE
 * nsCertType = client
 * keyUsage = digitalSignature, keyEncipherment
 * extendedKeyUsage = clientAuth
 * subjectAltName = @alt_names
 * [ alt_names ]
 * DNS.1 = vayananet.com
 *
 */

fun httpConnection(url: String) = (URL(url).openConnection() as HttpsURLConnection).apply {
    hostnameVerifier = object : HostnameVerifier {
        override fun verify(arg0: String, arg1: SSLSession) = true
    }

    val sslContext = SSLContext.getInstance("TLS").apply {
        val kmf = KeyManagerFactory.getInstance("SunX509", "SunJSSE").apply {
            val ks = KeyStore.getInstance("JKS").apply {
                load(javaClass.getResourceAsStream("/client_cert/vayanainteg.jks"), "changeit".toCharArray())
            }
            init(ks, "changeit".toCharArray())
        }
        init(kmf.keyManagers, null, SecureRandom())
    }
    sslSocketFactory = sslContext.socketFactory
}

fun main(args: Array<String>) {
//    val conn = httpConnection("https://b2b.ecsc.us.gxs.com:8443/invoke/gxs.https/receive")
    val conn = httpConnection("https://api-st.ril.com:8443/v5/vayana/funding/request")
    val iStream = conn.inputStream.bufferedReader()
    println("Received:")
    iStream.lines().forEach { println(it) }
}
