val scribe_java_version = "2.5.2"
val java_mail_version = "1.5.5"
val bouncycastle_version = "1.59"
val arrow_version = "0.8.2"

dependencies {
    compile("io.arrow-kt:arrow-core:$arrow_version")
    compile("io.arrow-kt:arrow-data:$arrow_version")
    compile("io.arrow-kt:arrow-instances-core:$arrow_version")
    compile("io.arrow-kt:arrow-instances-data:$arrow_version")
    compile("io.arrow-kt:arrow-syntax:$arrow_version")
    compile("io.arrow-kt:arrow-effects:$arrow_version")
    compile("io.arrow-kt:arrow-effects-instances:$arrow_version")

    compile("joda-time:joda-time:2.10")
    compile("com.github.scribejava:scribejava-core:$scribe_java_version")
    compile("com.github.scribejava:scribejava-apis:$scribe_java_version")
    compile("org.apache.oltu.oauth2:org.apache.oltu.oauth2.client:1.0.1")
    compile("javax.mail:javax.mail-api:$java_mail_version")
    compile("com.sun.mail:javax.mail:$java_mail_version")
    compile("org.kotyle:kylix:0.0.3")
    compile("com.fasterxml.jackson.module", "jackson-module-kotlin", "2.8.4")
    compile("org.apache.commons", "commons-text", "1.1")
    compile("commons-codec", "commons-codec", "1.10")
    compile("org.apache.pdfbox", "pdfbox", "2.0.8")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.1")
    compile("org.apache.httpcomponents", "httpclient", "4.5.5")
    compile("org.bouncycastle:bcprov-jdk15on:$bouncycastle_version")
    compile("org.bouncycastle:bcprov-ext-jdk15on:$bouncycastle_version")
    compile("org.bouncycastle:bcpkix-jdk15on:$bouncycastle_version")
    compile("com.google.guava:guava:15.0")
    compile("com.google.code.findbugs:jsr305:3.0.2")

    compile("com.github.salomonbrys.kotson", "kotson", "2.5.0")
    compile("org.http4k", "http4k-client-apache", "3.130.0")
}
