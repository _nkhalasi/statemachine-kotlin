group = "org.njk"
version = "1.0.0"

val kotlin_version = "1.3.50"

plugins {
    kotlin("jvm") version "1.3.50"
    kotlin("kapt") version "1.3.50"
    java
    idea
}


allprojects {
    repositories {
        mavenCentral()
        maven(url="https://dl.bintray.com/dnene/org.kotyle")
    }

    val logback_version = "1.1.6"
    val spek_version = "1.1.0-beta2"

    apply(plugin="org.jetbrains.kotlin.jvm")
    dependencies {
        implementation(kotlin("stdlib"))
        runtime(kotlin("reflect"))
        compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
        compile("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")

        compile("ch.qos.logback:logback-core:$logback_version")
        compile("ch.qos.logback:logback-classic:$logback_version")
        compile("org.slf4j:slf4j-api:1.7.12")

        testCompile("junit:junit:4.12")
        testCompile("org.jetbrains.kotlin:kotlin-test:$kotlin_version")
        testCompile("org.jetbrains.spek:spek-api:$spek_version")
        testCompile("org.jetbrains.spek:spek-junit-platform-engine:$spek_version")
        testCompile("org.junit.platform:junit-platform-runner:1.0.0-M3")
    }
}


tasks.compileKotlin {
    kotlinOptions.jvmTarget = "1.8"
}
tasks.compileTestKotlin {
    kotlinOptions.jvmTarget = "1.8"
}
